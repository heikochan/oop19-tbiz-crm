package com.tBiz.app;

import javax.swing.JFrame;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import com.tBiz.app.boundaries.MainFrame;
import com.tBiz.app.boundaries.SplashScreen;
import com.tBiz.app.entities.utils.DBSchemaGenerator;
import com.tBiz.app.entities.utils.HibernateUtil;

/**
 * Main Application
 */
public class App {

	private static MainFrame main = new MainFrame();
	private JWindow splashScreen = new JWindow();
	private static Session session;

	public App() {
		// initialize the splashscreen
		splashScreen = new SplashScreen(main);
		splashScreen.pack();
		splashScreen.setLocationRelativeTo(null);
		splashScreen.setVisible(true);

		// start a new thread that open the connection to the database
		new Thread(new Runnable() {

			@Override
			public void run() {
				// connect to the database
				connectDatabase();
				// meanwhile
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						// start drawing the mainframe
						createAndShowGUI();
					}
				});
			}
		}).start();
	}

	/**
	 * Open a session to the database via Hibernate
	 */
	public void connectDatabase() {
		try {
			this.configureDB();
			session = HibernateUtil.getSessionFactory().openSession();
			DBSchemaGenerator.populateDB();
			main.setupMenu();
			main.setLocationRelativeTo(null);
			main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// main.repaint();
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} catch (ExceptionInInitializerError e) {

		}
	}

	private void configureDB() {
		String configFileName = "hibernate.cfg.xml";

		// Create the ServiceRegistry from hibernate-xxx.cfg.xml
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure(configFileName).build();

		// Create a metadata sources using the specified service registry.
		Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

		SchemaExport export = DBSchemaGenerator.getSchemaExport();

		System.out.println("Drop Database..."); // Drop Database
		// dropDataBase(export, metadata);

		System.out.println("Create Database..."); // Create tables
		try {
			DBSchemaGenerator.createDataBase(export, metadata);
		} catch (HibernateException ex) {
			System.out.println(ex.toString());
		}
	}

	/**
	 * Show the MainFrame and the Splashscreen
	 */
	public void createAndShowGUI() {
		main.pack();
		main.setVisible(true);
		main.toFront();
		splashScreen.dispose();
	}

	/**
	 * application's main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				App application = new App();
			}
		});
	}

	public static MainFrame getMainFrame() {
		return main;
	}
}