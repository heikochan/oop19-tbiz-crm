package com.tBiz.app.controllers;

import java.util.HashMap;
import java.util.List;

import com.tBiz.app.boundaries.views.ActivityView;
import com.tBiz.app.boundaries.views.AddressView;
import com.tBiz.app.boundaries.views.ContactView;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.AddressEntity;
import com.tBiz.app.entities.ContactInfoEntity;
import com.tBiz.app.entities.CustomerEntity;
import com.tBiz.app.entities.repositories.ActivityRepository;
import com.tBiz.app.entities.repositories.AddressRepository;
import com.tBiz.app.entities.repositories.ContactRepository;

public class AddressController extends AbstractController {
	
	public AddressController() {
		view = new AddressView(this);
		model = new AddressRepository();
	}

	@Override
	public void viewList() {
		List<AddressEntity> addresses;
		if(this.getFilters().get("customer")!= null)
			this.viewList((int)this.getFilters().get("customer"));
		else {
			addresses = model.findAll(null);
			view.showList(addresses);
		}
	}

	public void viewList(int customer) {
		this.addFilter("customer", customer);
		HashMap<String,Object> criteria = new HashMap<String,Object>();
		criteria.put("customer", customer);
		List<AddressEntity> address = model.findBy(criteria);
		view.showList(address);
	}

	@Override
	public void viewDetails(int id) {
		System.out.println(id);
		AddressEntity address = (AddressEntity) model.find(id);
		view.showDetails(address);
	}

	public void addNew() {
		AddressEntity address = new AddressEntity();
		view.showDetails(address);
	}

	@Override
	public <T extends AbstractEntity> boolean isValid(T entity) {
		return true;
	}

	@Override
	public void setSearch(String search) {
		search = "%" + search + "%";
		HashMap<String,Object> searchClause = new HashMap<String,Object>();
		searchClause.put("street", search);
		searchClause.put("postal_code", search);
		this.addFilter("search", searchClause);
		
	}


}
