package com.tBiz.app.controllers;

import java.util.HashMap;

import javax.swing.JOptionPane;

import org.hibernate.PropertyValueException;

import com.tBiz.app.boundaries.views.AbstractView;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.repositories.AbstractRepository;
import com.tBiz.app.entities.utils.HibernateManager;

/**
 * 
 */
public abstract class AbstractController implements ControllerInterface {

	protected AbstractView view;
	protected AbstractRepository model;
	protected HibernateManager entityManager = new HibernateManager();
	protected HashMap<String, Object> filters = new HashMap<String, Object>();

	public AbstractController() {
	}

	/**
	 * Retrieve a List of Entities that matches the active filters and invoke the
	 * relative view to print data
	 */
	public abstract void viewList();

	/**
	 * Retrieve the entity by the Id parameter and invoke the relative view to print
	 * the detail
	 * 
	 * @param id ID of the entity to manage
	 */
	public abstract void viewDetails(int id);

	/**
	 * Invoke the view to manage a new entity
	 */
	public abstract void addNew();

	/**
	 * Check the Entity validation and save to DB
	 * 
	 * @param entity Entity to save
	 * @return boolean true if saved successfully/false if not valid or database
	 *         error
	 */
	public <T extends AbstractEntity> boolean saveDetails(T entity) {
		if (this.isValid(entity)) {
			boolean result = false;
			String msg = new String();
			try {
				result = this.entityManager.saveOrUpdate(entity);
			} catch (PropertyValueException ex) {
				msg = ex.getMessage();
			}
			System.out.println(result);
			if (result) {
				msg = "Dati salvati con successo!";
				JOptionPane.showConfirmDialog(null, msg, "Salvataggio dati", JOptionPane.CLOSED_OPTION);
			} else {
				msg = "Si è verificato un errore durant eil salvataggio dei dati" + "%n" + msg;
				JOptionPane.showConfirmDialog(null, msg, "Salvataggio dati", JOptionPane.CLOSED_OPTION,
						JOptionPane.ERROR_MESSAGE);
			}
			return result;
		} else {
			JOptionPane.showConfirmDialog(null, entity.getError().getErrorMsg(), "ATTENZIONE",
					JOptionPane.CLOSED_OPTION);
		}
		return false;
	}

	public AbstractRepository getModel() {
		return this.model;
	}

	/**
	 * Delete the entity after User confirm
	 * 
	 * @param entity Entity to delete
	 */
	public <T extends AbstractEntity> void delete(T entity) {
		int input = JOptionPane.showConfirmDialog(null, "Confermare l'eliminazione?", "ATTENZIONE!",
				JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		if (input == 0)
			this.entityManager.delete(entity);
	}

	/**
	 * Check entity's informations
	 * 
	 * @param entity Entity to Check
	 * @return boolean true if valid/false if not valid
	 */
	public abstract <T extends AbstractEntity> boolean isValid(T entity);

	/**
	 * Get active filters on the entity
	 * 
	 * @return HashMap<String,Object> containing active filters
	 */
	public HashMap<String, Object> getFilters() {
		return this.filters;
	}

	/**
	 * Add a new Filter to the Entity
	 * 
	 * @param key   Entity's attribute name on which apply the filter
	 * @param value Criteria
	 *              <p/>
	 * @param key   Accept also "search" and "array"
	 * @param value in this case, value will be an HashMap<String,Object> that
	 *              contains Attribute's name and Criteria in "or" clause
	 */
	public void addFilter(String key, Object value) {
		this.filters.put(key, value);
	}

	/**
	 * Remove an active filter from the list by the key
	 * 
	 * @param key filter to remove
	 */
	public void removeFilter(String key) {
		this.filters.remove(key);
	}

	/**
	 * Sets the fields to search in and add a "search" key in the filter
	 * 
	 * @param search text to search, will be add %text%
	 */
	public abstract void setSearch(String search);

	/**
	 * Remove the "search" key from the filter
	 */
	public void clearSearch() {
		this.removeFilter("search");
	}

}
