package com.tBiz.app.controllers;

import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.repositories.AbstractRepository;

public interface ControllerInterface {

	abstract void viewList();

	abstract void viewDetails(int id);

	abstract void addNew();

	<T extends AbstractEntity> boolean saveDetails(T entity);

	<T extends AbstractEntity> AbstractRepository<T> getModel();

	<T extends AbstractEntity> void delete(T entity);

	abstract <T extends AbstractEntity> boolean isValid(T entity);

}
