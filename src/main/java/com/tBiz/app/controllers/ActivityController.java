package com.tBiz.app.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tBiz.app.boundaries.views.ActivityView;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ActivityEntity;
import com.tBiz.app.entities.EntityError;
import com.tBiz.app.entities.repositories.ActivityRepository;

public class ActivityController extends AbstractController {

	public ActivityController() {
		view = new ActivityView(this);
		model = new ActivityRepository();
	}

	@Override
	public void viewList() {
		List<ActivityEntity> activities = null;
		if (this.getFilters().size() > 0)
			activities = model.findBy(this.filters);
		else {
			activities = model.findAll(null);
		}
		view.showList(activities);
	}

	public void viewDailyList() {
		HashMap<String, Object> criteria = new HashMap<String, Object>();
		LocalDate localDate = LocalDate.now();
		LocalDateTime ldtIni = LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
		criteria.put("startDate", ldtIni);
		List<ActivityEntity> activity = model.findBy(criteria);
		view.dateFilter = true;
		view.showList(activity);
	}

	public void viewWeeklyList() {
		List<ActivityEntity> activity = model.findAll(null);
		view.showList(activity);
	}

	@Override
	public void viewDetails(int id) {
		System.out.println(id);
		ActivityEntity activity = (ActivityEntity) model.find(id);
		view.showDetails(activity);
	}

	public void addNew() {
		ActivityEntity activity = new ActivityEntity();
		view.showDetails(activity);
	}

	@Override
	public <T extends AbstractEntity> boolean isValid(T entity) {
		boolean valid = true;
		Set<String> fields = new HashSet<String>();
		ActivityEntity act = (ActivityEntity) entity;
		EntityError error = null;

		if (act.getTitle() == "") {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Inserire un Titolo per l'attività");
			fields.add("title");
			error.setField(fields);
			valid = false;
		}
		if (valid && act.getCustomer() == null) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Specificare il Cliente/Lead");
			fields.add("customer");
			error.setField(fields);
			valid = false;
		}
		// Check that start date is before end date
		if (valid && act.getEndDate().compareTo(act.getStartDate()) < 0) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("La data di fine non può essere inferiore alla data di inizio");
			fields.add("startDate");
			fields.add("endDate");
			error.setField(fields);
			valid = false;
		}
		// Check if newly inserted date are not overlapping with other activities
		if (valid && this.model.areDateOverlapping(act.getStartDate(), act.getEndDate())) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Gli orari impostati si sovrappongono con altre attività");
			fields.add("startDate");
			fields.add("endDate");
			error.setField(fields);
			valid = false;
		}

		if (error != null)
			act.setError(error);
		return valid;
	}

	@Override
	public void setSearch(String search) {
		search = "%" + search + "%";
		HashMap<String, Object> searchClause = new HashMap<String, Object>();
		searchClause.put("title", search);
		searchClause.put("description", search);
		this.addFilter("search", searchClause);
	}

}
