package com.tBiz.app.controllers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tBiz.app.boundaries.views.CustomerView;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.CustomerEntity;
import com.tBiz.app.entities.EntityError;
import com.tBiz.app.entities.repositories.CustomerRepository;

public class CustomerController extends AbstractController {

	public CustomerController() {
		view = new CustomerView(this);
		model = new CustomerRepository();
	}

	public void viewList() {
		List<CustomerEntity> customers = model.findBy(this.filters);
		view.showList(customers);
		
	}

	@Override
	public void viewDetails(int id) {
		CustomerEntity customer = (CustomerEntity) model.find(id);
		view.showDetails(customer);
	}

	public void addNew() {
		CustomerEntity customer = new CustomerEntity();
		view.showDetails(customer);
	}

	@Override
	public boolean isValid(AbstractEntity customer) {
		boolean valid = true;
		Set<String> fields = new HashSet<String>();
		EntityError error = null;
		String firstname = ((CustomerEntity) customer).getFirstname();
		String lastname = ((CustomerEntity) customer).getLastname();
		String corpName = ((CustomerEntity) customer).getCorporateName();
		// If one beetween firstname and lastname is compiled, must be entered also the
		// other field
		if ((firstname.equals("") || lastname.equals("")) && (!(firstname + lastname).equals(""))) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Specificare Nome e Cognome della Persona");
			fields.add("firstname");
			fields.add("lastname");
			error.setField(fields);
			valid = false;
		}
		if (valid && !corpName.equals("") && !(firstname + lastname).equals("")) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Incongruenza nei dati: indicare Ragione Sociale O Nome e Cognome");
			fields.add("firstname");
			fields.add("lastname");
			fields.add("corporatename");
			error.setField(fields);
			valid = false;
		}
		if (valid && ((CustomerEntity) customer).isAcquired()
				&& ((CustomerEntity) customer).getAcquisitionDate() == null) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Indicare la data di acquisizione");
			fields.add("acquisitionDate");
			error.setField(fields);
			valid = false;
		}

		if (error != null)
			customer.setError(error);

		return valid;
	}
	
	@Override
	public void setSearch(String search) {
		HashMap<String,Object> searchClause = new HashMap<String,Object>();
		search = "%" + search + "%";
		searchClause.put("firstname", search);
		searchClause.put("lastname", search);
		searchClause.put("corporateName",search);
		this.addFilter("search", searchClause);
	}

}
