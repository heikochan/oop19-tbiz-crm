package com.tBiz.app.controllers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.tBiz.app.boundaries.views.ContactView;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ContactInfoEntity;
import com.tBiz.app.entities.EntityError;
import com.tBiz.app.entities.repositories.ContactRepository;

public class ContactController extends AbstractController {

	public ContactController() {
		view = new ContactView(this);
		model = new ContactRepository();
	}

	@Override
	public void viewList() {
		List<ContactInfoEntity> contacts=null;
		if(this.getFilters().size()>0)
			contacts = model.findBy(this.filters);
		else {
			contacts = model.findAll(null);
		}
		view.showList(contacts);
	}

	
	@Override
	public void viewDetails(int id) {
		System.out.println(id);
		ContactInfoEntity contact = (ContactInfoEntity) model.find(id);
		view.showDetails(contact);
	}

	public void addNew() {
		ContactInfoEntity contact = new ContactInfoEntity();
		view.showDetails(contact);
	}

	@Override
	public <T extends AbstractEntity> boolean isValid(T entity) {
		boolean valid = true;
		java.util.Set<String> fields = new HashSet<String>();
		EntityError error = null;
		ContactInfoEntity cinfo = (ContactInfoEntity) entity;

		/*
		 * if (cinfo.getCustomers() == null) { error = new EntityError();
		 * error.setErrorCode(0);
		 * error.setErrorMsg("Indicare il Cliente di appartenenza");
		 * fields.add("customers"); error.setField(fields); valid = false; }
		 */
		if (valid && cinfo.getContactType() == null) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Specificare il tipo di contatto");
			fields.add("contactType");
			error.setField(fields);
			valid = false;
		}

		return valid;
	}

	@Override
	public void setSearch(String search) {
		search = "%" + search + "%";
		HashMap<String,Object> searchClause = new HashMap<String,Object>();
		searchClause.put("name",search);
		searchClause.put("value", search);
		this.addFilter("search", searchClause);
	}

}
