package com.tBiz.app.controllers;

import java.util.List;

import com.tBiz.app.boundaries.views.SimpleTablesView;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.SectorEntity;
import com.tBiz.app.entities.repositories.SimpleTablesRepository;

public class SimpleTablesController<T extends AbstractEntity> extends AbstractController {

	private T entity;
	private String caption;

	public SimpleTablesController(T entity) {
		this.entity = entity;
		if (entity.getClass().isInstance(new ContactTypeEntity()))
			caption = "Tipo Contatti";
		else if (entity.getClass().isInstance(new ActivityTypeEntity()))
			caption = "Tipo Attività";
		else if (entity.getClass().isInstance(new SectorEntity()))
			caption = "Settori";

		model = new SimpleTablesRepository(entity.getClass());
		view = new SimpleTablesView(this, caption);
	}

	public void viewList() {
		List result = model.findAll(null);
		view.showList(result);
	}

	public void viewDetails(int id) {
		T entity = (T) model.find(id);
		view.showDetails(entity);
	}

	public void addNew() {
		T entity = null;
		try {
			entity = (T) this.entity.getClass().newInstance();
		}catch(IllegalAccessException e) {
			
		}catch(InstantiationException e) {
			
		}
		view.showDetails(entity);
	}

	@Override
	public <T extends AbstractEntity> boolean isValid(T entity) {
		return true;
	}

	@Override
	public void setSearch(String search) {
	}

}
