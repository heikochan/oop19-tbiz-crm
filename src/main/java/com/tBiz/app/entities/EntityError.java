package com.tBiz.app.entities;

import java.util.Set;

public class EntityError {
	
	private int errorCode=0;
	private String errorMsg="";
	private Set<String> field=null;
	
	public EntityError() {
		super();
	}
	public EntityError(int errorCode, String errorMsg, Set<String> field) {
		super();
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
		this.field = field;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public Set<String> getField() {
		return field;
	}
	public void setField(Set<String> field) {
		this.field = field;
	}
	
}
