package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "activity_type", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class ActivityTypeEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "name", nullable = false, length = 50)
	private String name;

	/*
	 * @OneToMany(targetEntity = ActivityEntity.class, orphanRemoval = true)
	 * 
	 * @JoinColumn(name = "activityType")
	 * 
	 * @OnDelete(action = OnDeleteAction.CASCADE) private Set activities;
	 */

	/**
	 * @param name
	 */
	public ActivityTypeEntity(String name) {
		super();
		this.name = name;
	}

	/**
	 * @param id
	 * @param name
	 * @param activities
	 */
	public ActivityTypeEntity(int id, String name/* , Set activities */) {
		super();
		this.id = id;
		this.name = name;
		// this.activities = activities;
	}

	public ActivityTypeEntity() {
		super();
	}

	/**
	 * @return this.the id
	 */
	@Override
	public int getId() {
		return this.id;
	}

	/**
	 * @return this.the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return this.the activities
	 */
	/*
	 * public Set getActivities() { return this.activities; }
	 */

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return this.name;
	}

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
		this.setName(description);
	}

	/**
	 * @param activities the activities to set
	 */
	/*
	 * public void setActivities(Set activities) { this.activities = activities; }
	 */

}
