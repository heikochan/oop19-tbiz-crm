package com.tBiz.app.entities;

import java.util.HashMap;

public abstract class AbstractEntity implements EntityInterface {

	// document state ( inserted, deleted, modified etc )
	private String state = "";

	private EntityError error = null;

	public AbstractEntity() {
		super();
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public EntityError getError() {
		return error;
	}

	public void setError(EntityError error) {
		this.error = error;
	}

	public abstract int getId();

	public abstract String getDescription();

	public abstract void setDescription(String description);

	public HashMap<Integer, String> getObject() {
		HashMap<Integer, String> ret = new HashMap<Integer, String>();
		ret.put(this.getId(), this.getDescription());
		return ret;
	}

	/*
	 * public boolean save() { Session session = null; Transaction transaction =
	 * null; // default value of returned variable boolean bSaved = false; try { //
	 * open the session session = HibernateUtil.getSession(); // get the transaction
	 * transaction = session.getTransaction(); // begin the transaction
	 * transaction.begin(); // try to save the element session.persist(this); //
	 * element saved bSaved = true; } catch (Exception ex) // otherwise { // in case
	 * of exception but transaction open if (transaction != null)
	 * transaction.rollback(); // rollback } finally { // in the end, close the
	 * session if (session != null) session.close(); } // return return bSaved;
	 * 
	 * }
	 */

	/*
	 * public boolean delete() { Session session = null; Transaction transaction =
	 * null; // default value of returned variable boolean bDeleted = false; try {
	 * // open the session session = HibernateUtil.getSession(); // get the
	 * transaction transaction = session.getTransaction(); // begin the transaction
	 * transaction.begin(); // try to delete the element session.delete(this); //
	 * element saved bDeleted = true; } catch (Exception ex) // otherwise { // in
	 * case of exception but transaction open if (transaction != null)
	 * transaction.rollback(); // rollback } finally { // in the end, close the
	 * session if (session != null) session.close(); } return bDeleted; }
	 */
}
