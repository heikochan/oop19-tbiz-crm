package com.tBiz.app.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.tBiz.app.boundaries.DateLabelFormatter;

@Entity
@Table(name = "customer", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class CustomerEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "firstname", length = 50)
	private String firstname;

	@Column(name = "lastname", length = 50)
	private String lastname;

	@Column(name = "corporate_name")
	private String corporateName;

	@Column(name = "first_contact_date")
	private LocalDate firstContactDate;

	@Column(name = "acquisition_date")
	private LocalDate acquisitionDate;

	@Column(name = "is_acquired", columnDefinition = "boolean default false")
	private boolean acquired;

	@Column(name = "fiscal_code", length = 50)
	private String fiscalCode;

	@Column(name = "vat_number", length = 50)
	private String vatNumber;

	@ManyToOne
	@JoinColumn(name = "sector_id", nullable = true)
	private SectorEntity sector;

	/**
	 * @param firstname
	 * @param lastname
	 * @param corporate_name
	 * @param first_contact_date
	 * @param acquisition_date
	 * @param is_acquired
	 * @param fiscal_code
	 * @param vat_number
	 */
	public CustomerEntity(String firstname, String lastname, String corporate_name, LocalDate first_contact_date,
			LocalDate acquisition_date, boolean is_acquired, String fiscal_code, String vat_number) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.corporateName = corporate_name;
		this.firstContactDate = first_contact_date;
		this.acquisitionDate = acquisition_date;
		this.acquired = is_acquired;
		this.fiscalCode = fiscal_code;
		this.vatNumber = vat_number;
	}

	public CustomerEntity() {
		super();
	}

	/**
	 * @return this.the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return this.the firstname
	 */
	public String getFirstname() {
		return this.firstname;
	}

	/**
	 * @return this.the lastname
	 */
	public String getLastname() {
		return this.lastname;
	}

	/**
	 * @return this.the corporate_name
	 */
	public String getCorporateName() {
		return this.corporateName;
	}

	/**
	 * @return this.the first_contact_date
	 */
	public LocalDate getFirstContactDate() {
		return this.firstContactDate;
	}

	/**
	 * @return this.the acquisition_date
	 */
	public LocalDate getAcquisitionDate() {
		return this.acquisitionDate;
	}

	/**
	 * @return this.the is_acquired
	 */
	public boolean isAcquired() {
		return this.acquired;
	}

	/**
	 * @return this.the fiscal_code
	 */
	public String getFiscalCode() {
		return this.fiscalCode;
	}

	/**
	 * @return this.the vat_number
	 */
	public String getVatNumber() {
		return this.vatNumber;
	}

	public SectorEntity getSector() {
		return this.sector;
	}

	/**
	 * @return the contacts
	 */
	/*
	 * public Set getContacts() { return this.contacts; }
	 * 
	 * /**
	 * 
	 * @return the sectors
	 */
	/*
	 * public Set getSectors() { return this.sectors; }
	 * 
	 * /**
	 * 
	 * @return the addresses
	 */
	/*
	 * public Set getAddresses() { return this.addresses; }
	 */

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @param corporate_name the corporate_name to set
	 */
	public void setCorporateName(String corporate_name) {
		this.corporateName = corporate_name;
	}

	/**
	 * @param first_contact_date the first_contact_date to set
	 */
	public void setFirstContactDate(LocalDate first_contact_date) {
		this.firstContactDate = first_contact_date;
	}

	/**
	 * @param acquisition_date the acquisition_date to set
	 */
	public void setAcquisitionDate(LocalDate acquisition_date) {
		this.acquisitionDate = acquisition_date;
	}

	/**
	 * @param is_acquired the is_acquired to set
	 */
	public void setAcquired(boolean is_acquired) {
		this.acquired = is_acquired;
	}

	/**
	 * @param fiscal_code the fiscal_code to set
	 */
	public void setFiscalCode(String fiscal_code) {
		this.fiscalCode = fiscal_code;
	}

	/**
	 * @param vat_number the vat_number to set
	 */
	public void setVatNumber(String vat_number) {
		this.vatNumber = vat_number;
	}

	public void setSector(SectorEntity sector) {
		this.sector = sector;
	}

	@Override
	public String getDescription() {
		String ret = new String();
		if (this.corporateName != null && !this.corporateName.isEmpty())
			ret = this.corporateName;
		else
			ret = this.lastname.trim() + " " + this.firstname.trim();
		return ret;
	}

	public String getAcquisitionDateFormatted() {
		String dt = null;

		dt = DateLabelFormatter.formatDate(this.acquisitionDate, "dd-MM-yyyy");

		return dt;
	}

	public String getFirstContactDateFormatted() {
		String dt = null;

		dt = DateLabelFormatter.formatDate(this.firstContactDate, "dd-MM-yyyy");

		return dt;
	}

	@Override
	public void setDescription(String description) {
	}

	/**
	 * @param contacts the contacts to set
	 */
	/*
	 * public void setContacts(Set contacts) { this.contacts = contacts; }
	 * 
	 * /**
	 * 
	 * @param sectors the sectors to set
	 */
	/*
	 * public void setSectors(Set sectors) { this.sectors = sectors; }
	 */

	/**
	 * @param sectors the sectors to set
	 */
	/*
	 * public void setAddresses(Set addresses) { this.addresses = addresses; }
	 */

}
