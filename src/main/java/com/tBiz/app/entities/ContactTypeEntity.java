/**
 * 
 */
package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "contact_type", uniqueConstraints = { @UniqueConstraint(columnNames = "id"),
		@UniqueConstraint(columnNames = "name") })
public class ContactTypeEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "name", unique = true, length = 50)
	private String name;

	@Column(name = "notes", columnDefinition = "TEXT", length = 255)
	private String notes;

	/*
	 * @OneToMany
	 * 
	 * @JoinColumn(name = "contact_type_id", nullable = false)
	 * 
	 * @OnDelete(action = OnDeleteAction.CASCADE) private Set contacts;
	 */

	/**
	 * @param name
	 */
	public ContactTypeEntity(String name) {
		super();
		this.name = name;
	}

	/**
	 * @param id
	 * @param name
	 * @param notes
	 */
	public ContactTypeEntity(int id, String name, String notes) {
		super();
		this.id = id;
		this.name = name;
		this.notes = notes;
	}

	public ContactTypeEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	@Override
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return this.notes;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String getDescription() {
		return this.getName();
	}

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
		this.setName(description);
	}

}