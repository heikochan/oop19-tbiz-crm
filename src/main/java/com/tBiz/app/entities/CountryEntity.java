package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "country", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class CountryEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "name", nullable = false, length = 50)
	private String name;

	/**
	 * @param name
	 */
	public CountryEntity(String name) {
		super();
		this.name = name;
	}

	/**
	 * @param id
	 * @param name
	 */
	public CountryEntity(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public CountryEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return this.name;
	}

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
		// TODO Auto-generated method stub

	}

}
