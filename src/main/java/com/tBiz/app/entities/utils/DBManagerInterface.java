package com.tBiz.app.entities.utils;

import com.tBiz.app.entities.AbstractEntity;

interface DBManagerInterface<E extends AbstractEntity> {

	public boolean saveOrUpdate(E entity);

	public boolean delete(E entity);
}
