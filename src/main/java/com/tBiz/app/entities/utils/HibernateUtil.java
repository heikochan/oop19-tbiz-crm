package com.tBiz.app.entities.utils;

import javax.persistence.EntityManager;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	/*
	 * private static final SessionFactory sessionFactory;
	 * 
	 * static { try { sessionFactory = new
	 * Configuration().configure().buildSessionFactory(); //
	 * "/home/stefania/eclipse-workspace/tBizCRM/src/main/resources/hibernate/hibernate.cfg.xml"
	 * 
	 * } catch (Throwable ex) { // Log exception! throw new
	 * ExceptionInInitializerError(ex); } }
	 * 
	 * public static Session getSession() throws HibernateException { return
	 * sessionFactory.openSession(); }
	 * 
	 * public static void closeSession() { // Close caches and connection pools
	 * getSession().close(); }
	 */
	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static final EntityManager entityManager = sessionFactory.createEntityManager();

	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new Configuration().configure().buildSessionFactory();

		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		// Close caches and connection pools
		getSessionFactory().close();
	}

	public static EntityManager getEntityManager() {
		return entityManager;
	}

}
