package com.tBiz.app.entities.utils;

import java.util.TimeZone;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tBiz.app.entities.AbstractEntity;

public class HibernateManager<E extends AbstractEntity> implements DBManagerInterface {

	protected static Session session;
	private static Transaction transaction;

	public HibernateManager() {
	}

	public static void openSession() {
		try {
			session = HibernateUtil.getSessionFactory().withOptions().jdbcTimeZone(TimeZone.getTimeZone("UTC"))
					.openSession();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}

	protected static void beginTransaction() {
		if (!session.isConnected())
			openSession();
		try {
			transaction = session.beginTransaction();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

	}

	protected static void closeSession() {
		try {
			session.getTransaction().commit();
			System.out.println("Modifiche committate");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static Transaction getTransaction() {
		return transaction;
	}

	public static Session getSession() {
		openSession();
		return session;
	}

	protected AbstractEntity load(Class<E> entityClass, Integer id) {
		AbstractEntity entity = getSession().get(entityClass, id);
		closeSession();
		return entity;
	}

	@Override
	public boolean saveOrUpdate(AbstractEntity entity) {
		boolean result = false;
		try {
			openSession();
			beginTransaction();
			session.saveOrUpdate((E) entity);
			result = true;
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			closeSession();
			return result;
		}
	};

	@Override
	public boolean delete(AbstractEntity entity) {
		boolean result = false;
		try {
			openSession();
			beginTransaction();
			session.delete((E) entity);
			result = true;
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			closeSession();
			return result;
		}
	}

}
