package com.tBiz.app.entities.utils;

import java.io.File;
import java.util.EnumSet;
import java.util.TimeZone;

import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.repositories.ActivityTypeRepository;

public class DBSchemaGenerator {

	public static final String SCRIPT_FILE = "exportScript.sql";

	public static SchemaExport getSchemaExport() {

		SchemaExport export = new SchemaExport();
		// Script file.
		File outputFile = new File(SCRIPT_FILE);
		String outputFilePath = outputFile.getAbsolutePath();

		System.out.println("Export file: " + outputFilePath);

		export.setDelimiter(";");
		export.setOutputFile(outputFilePath);

		// No Stop

		/*
		 * if Error export.setHaltOnError(false); //
		 */
		return export;
	}

	/*
	 * public static void dropDataBase(SchemaExport export, Metadata metadata) { *
	 * //TargetType.DATABASE - Execute on Databse // //TargetType.SCRIPT - Write
	 * Script file. // //TargetType.STDOUT - write log to Console. //
	 * EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE,
	 * TargetType.SCRIPT, TargetType.STDOUT);
	 * 
	 * export.drop(targetTypes, metadata); }
	 */
	//
	public static void createDataBase(SchemaExport export, Metadata metadata) {
		// TargetType.DATABASE - Execute on Databse // TargetType.SCRIPT - Write
		// Script file. // TargetType.STDOUT - Write log to Console.

		EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);

		SchemaExport.Action action = SchemaExport.Action.CREATE; //
		export.execute(targetTypes, action, metadata);

		System.out.println("Export OK");

	}

	public static void populateDB() {
		Session session = HibernateUtil.getSessionFactory().withOptions().jdbcTimeZone(TimeZone.getTimeZone("UTC"))
				.openSession();
		try {
			ActivityTypeEntity ac = (ActivityTypeEntity) (new ActivityTypeRepository()).find(1);
			if (ac != null && ac.getId() != 0)
				return;
			Transaction transaction = session.beginTransaction();
			Query activityTypeQuery = session
					.createNativeQuery("INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (1,'Email'); "
							+ "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (2,'Telefonata'); "
							+ "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (3,'Incontro');  "
							+ "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (4,'Call online');  "
							+ "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (5,'Preventivo');  "
							+ "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (6,'Analisi e Documentazione'); ");
			activityTypeQuery.executeUpdate();

			Query contactTypeQuery = session
					.createNativeQuery("INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (1,'Telefono fisso'); \n"
							+ "INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (2,'Cellulare'); \n"
							+ "INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (3,'Email Personale'); \n"
							+ "INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (4,'Email Aziendale'); \n"
							+ "INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (5,'Contatto Skype'); \n"
							+ "INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (6,'Contatto Whatsapp'); \n"
							+ "INSERT INTO CONTACT_TYPE  (ID , NAME) VALUES (7,'Contatto Telegram'); ");
			contactTypeQuery.executeUpdate();

			Query sectorQuery = session
					.createNativeQuery("INSERT INTO SECTOR  (ID , NAME) VALUES (1,'Manifatturiero'); \n"
							+ "			INSERT INTO SECTOR  (ID , NAME) VALUES (2,'Pulizie'); \n"
							+ "			INSERT INTO SECTOR  (ID , NAME) VALUES (3,'Terme'); \n"
							+ "			INSERT INTO SECTOR  (ID , NAME) VALUES (4,'Cosmetico'); \n"
							+ "			INSERT INTO SECTOR  (ID , NAME) VALUES (5,'Sportivo'); ");
			sectorQuery.executeUpdate();

			Query provinceQuery = session
					.createNativeQuery("			INSERT INTO COUNTRY  (ID , NAME) VALUES (1,'Italia'); \n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (1,'01','Piemonte','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (2,'02','Valle d''Aosta/Vallée d''Aoste','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (3,'03','Lombardia','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (4,'04','Trentino-Alto Adige/Südtirol','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (5,'05','Veneto','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (6,'06','Friuli-Venezia Giulia','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (7,'07','Liguria','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (8,'08','Emilia-Romagna','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (9,'09','Toscana','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (10,'10','Umbria','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (11,'11','Marche','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (12,'12','Lazio','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (13,'13','Abruzzo','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (14,'14','Molise','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (15,'15','Campania','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (16,'16','Puglia','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (17,'17','Basilicata','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (18,'18','Calabria','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (19,'19','Sicilia','1');\n"
							+ "			INSERT INTO REGION(id,code,name,country_id) VALUES (20,'20','Sardegna','1');\n"
							+ "			INSERT INTO PROVINCE  (NAME , CODE , REGION_ID ) VALUES\n"
							+ "			('Torino', 'TO', 1),\n" + "			('Vercelli', 'VC', 1),\n"
							+ "			('Novara', 'NO', 1),\n" + "			('Cuneo', 'CN', 1),\n"
							+ "			('Asti', 'AT', 1),\n" + "			('Alessandria', 'AL', 1),\n"
							+ "			('Biella', 'BI', 1),\n" + "			('Verbano-Cusio-Ossola', 'VB', 1),\n"
							+ "			('Valle d Aosta/Vallée d Aoste', 'AO', 2),\n"
							+ "			('Varese', 'VA', 3),\n" + "			('Como', 'CO', 3),\n"
							+ "			('Sondrio', 'SO', 3),\n" + "			('Milano', 'MI', 3),\n"
							+ "			('Bergamo', 'BG', 3),\n" + "			('Brescia', 'BS', 3),\n"
							+ "			('Pavia', 'PV', 3),\n" + "			('Cremona', 'CR', 3),\n"
							+ "			('Mantova', 'MN', 3),\n" + "			('Lecco', 'LC', 3),\n"
							+ "			('Lodi', 'LO', 3),\n" + "			('Monza e della Brianza', 'MB', 3),\n"
							+ "			('Bolzano/Bozen', 'BZ', 4),\n" + "			('Trento', 'TN', 4),\n"
							+ "			('Verona', 'VR', 5),\n" + "			('Vicenza', 'VI', 5),\n"
							+ "			('Belluno', 'BL', 5),\n" + "			('Treviso', 'TV', 5),\n"
							+ "			('Venezia', 'VE', 5),\n" + "			('Padova', 'PD', 5),\n"
							+ "			('Rovigo', 'RO', 5),\n" + "			('Udine', 'UD', 6),\n"
							+ "			('Gorizia', 'GO', 6),\n" + "			('Trieste', 'TS', 6),\n"
							+ "			('Pordenone', 'PN', 6),\n" + "			('Imperia', 'IM', 7),\n"
							+ "			('Savona', 'SV', 7),\n" + "			('Genova', 'GE', 7),\n"
							+ "			('La Spezia', 'SP', 7),\n" + "			('Piacenza', 'PC', 8),\n"
							+ "			('Parma', 'PR', 8),\n" + "			('Reggio nell Emilia', 'RE', 8),\n"
							+ "			('Modena', 'MO', 8),\n" + "			('Bologna', 'BO', 8),\n"
							+ "			('Ferrara', 'FE', 8),\n" + "			('Ravenna', 'RA', 8),\n"
							+ "			('Forlì-Cesena', 'FC', 8),\n" + "			('Rimini', 'RN', 8),\n"
							+ "			('Massa-Carrara', 'MS', 9),\n" + "			('Lucca', 'LU', 9),\n"
							+ "			('Pistoia', 'PT', 9),\n" + "			('Firenze', 'FI', 9),\n"
							+ "			('Livorno', 'LI', 9),\n" + "			('Pisa', 'PI', 9),\n"
							+ "			('Arezzo', 'AR', 9),\n" + "			('Siena', 'SI', 9),\n"
							+ "			('Grosseto', 'GR', 9),\n" + "			('Prato', 'PO', 9),\n"
							+ "			('Perugia', 'PG', 10),\n" + "			('Terni', 'TR', 10),\n"
							+ "			('Pesaro e Urbino', 'PU', 11),\n" + "			('Ancona', 'AN', 11),\n"
							+ "			('Macerata', 'MC', 11),\n" + "			('Ascoli Piceno', 'AP', 11),\n"
							+ "			('Fermo', 'FM', 11),\n" + "			('Viterbo', 'VT', 12),\n"
							+ "			('Rieti', 'RI', 12),\n" + "			('Roma', 'RM', 12),\n"
							+ "			('Latina', 'LT', 12),\n" + "			('Frosinone', 'FR', 12),\n"
							+ "			('L Aquila', 'AQ', 13),\n" + "			('Teramo', 'TE', 13),\n"
							+ "			('Pescara', 'PE', 13),\n" + "			('Chieti', 'CH', 13),\n"
							+ "			('Campobasso', 'CB', 14),\n" + "			('Isernia', 'IS', 14),\n"
							+ "			('Caserta', 'CE', 15),\n" + "			('Benevento', 'BN', 15),\n"
							+ "			('Napoli', 'NA', 15),\n" + "			('Avellino', 'AV', 15),\n"
							+ "			('Salerno', 'SA', 15),\n" + "			('Foggia', 'FG', 16),\n"
							+ "			('Bari', 'BA', 16),\n" + "			('Taranto', 'TA', 16),\n"
							+ "			('Brindisi', 'BR', 16),\n" + "			('Lecce', 'LE', 16),\n"
							+ "			('Barletta-Andria-Trani', 'BT', 16),\n" + "			('Potenza', 'PZ', 17),\n"
							+ "			('Matera', 'MT', 17),\n" + "			('Cosenza', 'CS', 18),\n"
							+ "			('Catanzaro', 'CZ', 18),\n" + "			('Reggio Calabria', 'RC', 18),\n"
							+ "			('Crotone', 'KR', 18),\n" + "			('Vibo Valentia', 'VV', 18),\n"
							+ "			('Trapani', 'TP', 19),\n" + "			('Palermo', 'PA', 19),\n"
							+ "			('Messina', 'ME', 19),\n" + "			('Agrigento', 'AG', 19),\n"
							+ "			('Caltanissetta', 'CL', 19),\n" + "			('Enna', 'EN', 19),\n"
							+ "			('Catania', 'CT', 19),\n" + "			('Ragusa', 'RG', 19),\n"
							+ "			('Siracusa', 'SR', 19),\n" + "			('Sassari', 'SS', 20),\n"
							+ "			('Nuoro', 'NU', 20),\n" + "			('Cagliari', 'CA', 20),\n"
							+ "			('Oristano', 'OR', 20),\n" + "			('Sud Sardegna', 'SU', 20);");
			provinceQuery.executeUpdate();

			Query cityQuery = session.createNativeQuery(
					"INSERT INTO CITY  (NAME, CODE, PROVINCE_ID) VALUES ('Acqualagna', 'A035', 60),\n"
							+ "('Apecchio', 'A327', 60),\n" + "('Belforte all Isauro', 'A740', 60),\n"
							+ "('Borgo Pace', 'B026', 60),\n" + "('Cagli', 'B352', 60),\n"
							+ "('Cantiano', 'B636', 60),\n" + "('Carpegna', 'B816', 60),\n"
							+ "('Cartoceto', 'B846', 60),\n" + "('Fano', 'D488', 60),\n"
							+ "('Fermignano', 'D541', 60),\n" + "('Fossombrone', 'D749', 60),\n"
							+ "('Fratte Rosa', 'D791', 60),\n" + "('Frontino', 'D807', 60),\n"
							+ "('Frontone', 'D808', 60),\n" + "('Gabicce Mare', 'D836', 60),\n"
							+ "('Gradara', 'E122', 60),\n" + "('Isola del Piano', 'E351', 60),\n"
							+ "('Lunano', 'E743', 60),\n" + "('Macerata Feltria', 'E785', 60),\n"
							+ "('Mercatello sul Metauro', 'F135', 60),\n" + "('Mercatino Conca', 'F136', 60),\n"
							+ "('Mombaroccio', 'F310', 60),\n" + "('Mondavio', 'F347', 60),\n"
							+ "('Mondolfo', 'F348', 60),\n" + "('Montecalvo in Foglia', 'F450', 60),\n"
							+ "('Monte Cerignone', 'F467', 60),\n" + "('Monteciccardo', 'F474', 60),\n"
							+ "('Montecopiolo', 'F478', 60),\n" + "('Montefelcino', 'F497', 60),\n"
							+ "('Monte Grimano Terme', 'F524', 60),\n" + "('Montelabbate', 'F533', 60),\n"
							+ "('Monte Porzio', 'F589', 60),\n" + "('Peglio', 'G416', 60),\n"
							+ "('Pergola', 'G453', 60),\n" + "('Pesaro', 'G479', 60),\n" + "('Petriano', 'G514', 60),\n"
							+ "('Piandimeleto', 'G551', 60),\n" + "('Pietrarubbia', 'G627', 60),\n"
							+ "('Piobbico', 'G682', 60),\n" + "('San Costanzo', 'H809', 60),\n"
							+ "('San Lorenzo in Campo', 'H958', 60),\n" + "('Sant Angelo in Vado', 'I287', 60),\n"
							+ "('Sant Ippolito', 'I344', 60),\n" + "('Sassofeltrio', 'I460', 60),\n"
							+ "('Serra Sant Abbondio', 'I654', 60),\n" + "('Tavoleto', 'L078', 60),\n"
							+ "('Tavullia', 'L081', 60),\n" + "('Urbania', 'L498', 60),\n" + "('Urbino', 'L500', 60),\n"
							+ "('Vallefoglia', 'M331', 60),\n" + "('Colli al Metauro', 'M380', 60),\n"
							+ "('Terre Roveresche', 'M379', 60),\n" + "('Sassocorvaro Auditore', 'M413', 60);");
			cityQuery.executeUpdate();

			try {
				transaction.commit();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		} catch (HibernateException e) {
			e.printStackTrace();

			HibernateUtil.closeSessionFactory();
		}

		/*
		 * HibernateUtil.getEntityManager()
		 * .createNativeQuery("INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (1,'Email'); "
		 * + "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (2,'Telefonata'); " +
		 * "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (3,'Incontro');  " +
		 * "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (4,'Call online');  " +
		 * "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (5,'Preventivo');  " +
		 * "INSERT INTO ACTIVITY_TYPE  (ID , NAME) VALUES (6,'Analisi e Documentazione'); "
		 * ) .executeUpdate();
		 */

	}
}
