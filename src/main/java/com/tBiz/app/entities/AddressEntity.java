package com.tBiz.app.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "address", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class AddressEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "street", nullable = false, length = 50)
	private String street;

	@Column(name = "postal_code", length = 10)
	private String postal_code;

	@ManyToOne
	@JoinColumn(name = "city_id", nullable = false)
	private CityEntity city;

	@ManyToOne
	@JoinColumn(name = "customer_id", nullable = false)
	private CustomerEntity customer;

	/**
	 * @param street
	 * @param city
	 * @param customer
	 */
	public AddressEntity(String street, CityEntity city, CustomerEntity customer) {
		super();
		this.street = street;
		this.city = city;
		this.customer = customer;
	}

	/**
	 * @param id
	 * @param street
	 * @param postal_code
	 * @param city
	 * @param customer
	 */
	public AddressEntity(int id, String street, String postal_code, CityEntity city, CustomerEntity customer) {
		super();
		this.id = id;
		this.street = street;
		this.postal_code = postal_code;
		this.city = city;
		this.customer = customer;
	}

	public AddressEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return this.street;
	}

	/**
	 * @return the postal_code
	 */
	public String getPostal_code() {
		return this.postal_code;
	}

	/**
	 * @return the city
	 */
	public CityEntity getCity() {
		return this.city;
	}

	/**
	 * @return the customer
	 */
	public CustomerEntity getCustomer() {
		return this.customer;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @param postal_code the postal_code to set
	 */
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(CityEntity city) {
		this.city = city;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public String getCityName() {
		return this.city.getDescription();
	}

	@Override
	public String getDescription() {
		return this.street;
	}

	public boolean isValid() {
		boolean valid = true;
		Set<String> fields = new HashSet<String>();
		EntityError error = null;

		if (this.customer == null) {
			error = new EntityError();
			error.setErrorCode(0);
			error.setErrorMsg("Indicare il Cliente di appartenenza");
			fields.add("customer");
			error.setField(fields);
			valid = false;
		}

		return valid;
	}

	@Override
	public void setDescription(String description) {
	}

}
