package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "region", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class RegionEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "name", nullable = false, length = 50)
	private String name;

	@Column(name = "code", length = 3)
	private String code;

	@ManyToOne
	@JoinColumn(name = "country_id", nullable = false)
	private CountryEntity country;

	/*
	 * @OneToMany(mappedBy = "region") private Set provinces;
	 */

	public RegionEntity(String name, CountryEntity country) {
		super();
		this.name = name;
		this.country = country;
	}

	/**
	 * @param name
	 * @param code
	 * @param country
	 */
	public RegionEntity(int id, String name, String code, CountryEntity country) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.country = country;
	}

	public RegionEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @return the country
	 */
	public CountryEntity getCountry() {
		return this.country;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(CountryEntity country) {
		this.country = country;
	}

	@Override
	public String getDescription() {
		return this.name;
	}

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
	}

}
