package com.tBiz.app.entities;

import java.util.HashMap;

interface EntityInterface {

	public String getState();

	public void setState(String state);

	public EntityError getError();

	public void setError(EntityError error);

	public abstract int getId();

	public abstract String getDescription();

	public abstract void setDescription(String description);

	public HashMap<Integer, String> getObject();

}