package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "city", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class CityEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	@Column(name = "code", length = 4)
	private String code;

	@ManyToOne
	@JoinColumn(name = "province_id", nullable = false)
	private ProvinceEntity province;

	/**
	 * @param name
	 * @param province
	 */
	public CityEntity(String name, ProvinceEntity province) {
		super();
		this.name = name;
		this.province = province;
	}

	/**
	 * @param id
	 * @param name
	 * @param code
	 * @param province
	 */
	public CityEntity(int id, String name, String code, ProvinceEntity province) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.province = province;
	}

	/**
	 * 
	 */
	public CityEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @return the province
	 */
	public ProvinceEntity getProvince() {
		return this.province;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(ProvinceEntity province) {
		this.province = province;
	}

	@Override
	public String getDescription() {
		return this.name;
	}

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
	}

}
