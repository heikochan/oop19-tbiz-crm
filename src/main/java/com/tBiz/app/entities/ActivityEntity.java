/**
 * 
 */
package com.tBiz.app.entities;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.tBiz.app.boundaries.DateLabelFormatter;

@Entity
@Table(name = "activity", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class ActivityEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "notes", columnDefinition = "TEXT")
	private String notes;

	@Column(name = "start_date")
	private LocalDateTime startDate;

	@Column(name = "end_date")
	private LocalDateTime endDate;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "activity_contact_id", nullable = false)
	private ContactInfoEntity contact;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "activity_customer_id", nullable = false)
	private CustomerEntity customer;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "activity_type_id", nullable = false)
	private ActivityTypeEntity activityType;

	/**
	 * @param title
	 * @param contact
	 * @param activityType
	 */
	public ActivityEntity(String title, CustomerEntity customer, ActivityTypeEntity activityType) {
		super();
		this.title = title;
		this.customer = customer;
		this.activityType = activityType;
	}

	/**
	 * @param id
	 * @param title
	 * @param description
	 * @param notes
	 * @param startDate
	 * @param endDate
	 * @param contact
	 * @param activityType
	 */
	public ActivityEntity(String title, String description, String notes, LocalDateTime startDate,
			LocalDateTime endDate, ContactInfoEntity contact, CustomerEntity customer,
			ActivityTypeEntity activityType) {
		super();
		this.title = title;
		this.description = description;
		this.notes = notes;
		this.startDate = startDate;
		this.endDate = endDate;
		this.contact = contact;
		this.customer = customer;
		this.activityType = activityType;
	}

	public ActivityEntity() {
		super();
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return this.notes;
	}

	/**
	 * @return the startDate
	 */
	public LocalDateTime getStartDate() {
		return this.startDate;
	}

	/**
	 * @return the endDate
	 */
	public LocalDateTime getEndDate() {
		return this.endDate;
	}

	/**
	 * @return the contact
	 */
	public ContactInfoEntity getContact() {
		return this.contact;
	}

	/**
	 * @return the customerfield
	 */
	public CustomerEntity getCustomer() {
		return this.customer;
	}

	/**
	 * @return the activityType
	 */
	public ActivityTypeEntity getActivityType() {
		return this.activityType;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(ContactInfoEntity contact) {
		this.contact = contact;
	}

	/**
	 * @param contact the contact to setfield
	 */
	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	/**
	 * @param activityType the activityType to set
	 */
	public void setActivityType(ActivityTypeEntity activityType) {
		this.activityType = activityType;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCustomerName() {
		String customerName = "";
		if (this.customer.getCorporateName() != null && !this.customer.getCorporateName().equals("")) {
			customerName = this.customer.getCorporateName();
		} else {
			customerName = this.customer.getLastname() + " " + this.customer.getFirstname();
		}
		return customerName;
	}

	public String getActivityTypeName() {
		return this.activityType.getName();
	}

	public String getContactName() {
		return this.contact.getName();
	}

	public String getStartDateFormatted() {
		String dt = null;
		dt = DateLabelFormatter.formatDateTime(this.startDate, "dd-MM-yyyy kk:ss");
		return dt;
	}

	public String getEndDateFormatted() {
		String dt = null;
		dt = DateLabelFormatter.formatDateTime(this.endDate, "dd-MM-yyyy kk:ss");
		return dt;
	}

}
