package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "province", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class ProvinceEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "code", length = 3)
	private String code;

	@ManyToOne
	@JoinColumn(name = "region_id", nullable = false)
	private RegionEntity region;

	/**
	 * @param name
	 * @param region
	 */
	public ProvinceEntity(String name, RegionEntity region) {
		super();
		this.name = name;
		this.region = region;
	}

	/**
	 * @param id
	 * @param name
	 * @param code
	 * @param region
	 */
	public ProvinceEntity(int id, String name, String code, RegionEntity region) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.region = region;
	}

	public ProvinceEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @return the region
	 */
	public RegionEntity getRegion() {
		return this.region;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param region the region to set
	 */
	public void setRegion(RegionEntity region) {
		this.region = region;
	}

	@Override
	public String getDescription() {
		return this.name;
	}

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
	}

}
