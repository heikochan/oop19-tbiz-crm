package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.ActivityEntity;
import com.tBiz.app.entities.CityEntity;

public class CityRepository extends AbstractRepository {

	public CityRepository() {
		super(CityEntity.class);
	}

}
