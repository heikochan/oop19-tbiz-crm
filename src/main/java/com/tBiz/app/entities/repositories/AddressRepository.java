package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.AddressEntity;

public class AddressRepository extends AbstractRepository {

	public AddressRepository() {
		super(AddressEntity.class);
	}

}
