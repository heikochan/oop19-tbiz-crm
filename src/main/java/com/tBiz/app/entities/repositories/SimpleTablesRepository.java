package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ContactInfoEntity;

public class SimpleTablesRepository <T extends AbstractEntity> extends AbstractRepository {

	public SimpleTablesRepository(Class<T> arg) {
		super(arg);
	}

}
