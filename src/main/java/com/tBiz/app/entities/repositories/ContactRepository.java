package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.ContactInfoEntity;

public class ContactRepository extends AbstractRepository {

	public ContactRepository() {
		super(ContactInfoEntity.class);
	}

}
