package com.tBiz.app.entities.repositories;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.utils.HibernateUtil;

// Generic class for common model procedures
public abstract class AbstractRepository<T extends AbstractEntity> /* implements ManagerInterface */ {

	protected EntityManager em = HibernateUtil.getEntityManager();
	protected CriteriaBuilder cb = this.em.getCriteriaBuilder();
	protected CriteriaQuery<T> critquery;
	protected Root<T> root;
	protected Class<T> entityClass;
	/*
	 * private String tableName = ""; private String whereClause = "";
	 */

	public AbstractRepository(Class<T> entClass) {
		this.entityClass = entClass;
		this.critquery = this.cb.createQuery(this.entityClass);
		this.root = this.critquery.from(this.entityClass);
	}

	public Class<T> getEntityClass() {
		return entityClass;
	}

	// General procedure for retriving entity by id
	public T find(int id) {
		T result = this.em.find(this.entityClass, id);
		return result;
	}

	public List<T> findAll(String[] orderBy) {
		this.critquery = this.cb.createQuery(this.entityClass);
		this.root = critquery.from(this.entityClass);
		this.critquery.select(root);
		TypedQuery<T> query = this.em.createQuery(this.critquery);
		List<T> entitiesList = query.getResultList();
		return entitiesList;
	}

	public List<T> findBy(HashMap<String, Object> criteria) {
		String[] order = null;
		return this.findBy(criteria, order);
	}

	public List<T> findBy(HashMap<String, Object> criteriaList, String[] orderBy) {
		// CriteriaBuilder builder = em.getCriteriaBuilder();
		System.out.println(this.entityClass.toString());
		this.critquery = this.cb.createQuery(this.entityClass);
		this.root = critquery.from(this.entityClass);

		Predicate final1 = getClauseExpression(criteriaList, "and");

		this.critquery.select(this.root).where(final1);
		TypedQuery<T> query = this.em.createQuery(this.critquery);
		List<T> entitiesList = query.getResultList();
		return entitiesList;
	}

	public boolean areDateOverlapping(LocalDateTime startDate, LocalDateTime endDate) {
		this.critquery = this.cb.createQuery(this.entityClass);
		this.root = critquery.from(this.entityClass);
		Predicate[] predicates = new Predicate[2];
		Predicate predStartDate = this.cb.greaterThanOrEqualTo(root.get("endDate"), startDate);
		Predicate predEndDate = this.cb.lessThanOrEqualTo(root.get("startDate"), endDate);
		this.critquery.select(this.root).where(this.cb.and(predStartDate, predEndDate));
		TypedQuery<T> query = this.em.createQuery(this.critquery);
		List<T> result = query.getResultList();
		return result.size() > 0 ? true : false;

	}

	protected Predicate getClauseExpression(HashMap<String, Object> criteria, String operator) {
		Predicate finalExpression = null;

		List<Predicate> predicates = new ArrayList<Predicate>();

		for (String propertyName : criteria.keySet()) {
			Predicate expression = null;
			if (propertyName.equals("array") || propertyName.equals("search"))
				expression = getClauseExpression((HashMap<String, Object>) criteria.get(propertyName), "or");
			else {
				String propertyClass = criteria.get(propertyName).getClass().getSimpleName();
				switch (propertyClass) {
				case "int":
				case "Integer":
					expression = this.cb.equal(this.root.get(propertyName), criteria.get(propertyName));
					break;
				case "Boolean":
				case "boolean":
					expression = this.cb.equal(this.root.get(propertyName),
							(criteria.get(propertyName).equals(true) ? 1 : 0));
					break;
				case "Date":
				case "date":
				case "Datetime":
				case "datetime":
				case "LocalDate":
					LocalDate dtIni = (LocalDate) criteria.get(propertyName);
					LocalDate dtFin = dtIni.plusDays(1);
					expression = this.cb.between(this.root.get(propertyName), dtIni, dtFin);
					break;
				case "LocalDateTime":
					LocalDateTime dttIni = (LocalDateTime) criteria.get(propertyName);
					LocalDateTime dttFin = dttIni.plusDays(1);
					expression = this.cb.between(this.root.get(propertyName), dttIni, dttFin);
					break;
				default:
					expression = this.cb.like(this.root.get(propertyName), criteria.get(propertyName).toString());
					break;
				}
			}
			predicates.add(expression);

		}

		if (operator.equals("and"))
			finalExpression = cb.and(predicates.toArray(new Predicate[0]));
		else if (operator.equals("or"))
			finalExpression = cb.or(predicates.toArray(new Predicate[0]));

		return finalExpression;
	}

}
