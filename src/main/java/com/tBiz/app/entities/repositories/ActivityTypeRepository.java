package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.ActivityEntity;
import com.tBiz.app.entities.ActivityTypeEntity;

public class ActivityTypeRepository extends AbstractRepository {

	public ActivityTypeRepository() {
		super(ActivityTypeEntity.class);
	}

}
