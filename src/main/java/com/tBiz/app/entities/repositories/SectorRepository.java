package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.ActivityEntity;
import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.SectorEntity;

public class SectorRepository extends AbstractRepository {

	public SectorRepository() {
		super(SectorEntity.class);
	}

}
