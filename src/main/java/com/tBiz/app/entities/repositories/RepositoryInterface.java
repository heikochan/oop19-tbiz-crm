package com.tBiz.app.entities.repositories;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import com.tBiz.app.entities.AbstractEntity;

interface RepositoryInterface<T extends AbstractEntity> {

	public Class<T> getEntityClass();

	public T find(int id);

	public List<T> findAll(String[] orderBy);

	public List<T> findBy(HashMap<String, Object> criteria);

	public List<T> findBy(HashMap<String, Object> criteriaList, String[] orderBy);

	public boolean areDateOverlapping(LocalDateTime startDate, LocalDateTime endDate);
}
