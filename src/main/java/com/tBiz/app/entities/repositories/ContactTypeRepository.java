package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.ContactInfoEntity;
import com.tBiz.app.entities.ContactTypeEntity;

public class ContactTypeRepository extends AbstractRepository {

	public ContactTypeRepository() {
		super(ContactTypeEntity.class);
	}

}
