package com.tBiz.app.entities.repositories;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import com.tBiz.app.entities.CustomerEntity;

public class CustomerRepository extends AbstractRepository {

	final Class<CustomerEntity> CUSTOMER_CLASS = CustomerEntity.class;

	public CustomerRepository() {
		super(CustomerEntity.class);
		/*
		 * this.em.getCriteriaBuilder(); this.critquery =
		 * cb.createQuery(this.entityClass); this.root =
		 * critquery.from(this.entityClass);
		 */
	}

	public List<CustomerEntity> findAllAcquired(String[] orderBy) {
		CriteriaQuery<CustomerEntity> select = this.critquery.select(this.root);
		select.where(this.cb.equal(this.root.get("acquired"), 1));
		TypedQuery<CustomerEntity> q = this.em.createQuery(critquery);
		List<CustomerEntity> results = q.getResultList();
		return results;
	}

	public List<CustomerEntity> findAllLeads(String[] orderBy) {
		CriteriaQuery<CustomerEntity> select = this.critquery.select(this.root);
		select.where(this.cb.equal(this.root.get("acquired"), 0));
		TypedQuery<CustomerEntity> q = this.em.createQuery(critquery);
		List<CustomerEntity> results = q.getResultList();
		return results;
	}

	public List<CustomerEntity> findByName(String name, HashMap<String, String> orderByData) {
		CriteriaQuery<CustomerEntity> query = this.critquery.select(this.root);
		query.where(this.cb.equal(this.root.get("acquired"), 0));
		List orderClause = null;
		if (orderByData.size() > 0) {
			Iterator it = orderByData.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, String> sorting = (Entry<String, String>) it.next();
				if (sorting.getValue() == "ASC") {
					orderClause.add(this.cb.asc(root.get(sorting.getKey())));
				} else {
					orderClause.add(this.cb.desc(root.get(sorting.getKey())));
				}
			}
		}
		if (orderClause != null || orderClause.size() > 0) {
			query.orderBy(orderClause);
		}
		TypedQuery<CustomerEntity> q = this.em.createQuery(critquery);
		List<CustomerEntity> results = q.getResultList();
		return results;
	}

}
