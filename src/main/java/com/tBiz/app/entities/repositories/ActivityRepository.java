package com.tBiz.app.entities.repositories;

import com.tBiz.app.entities.ActivityEntity;

public class ActivityRepository extends AbstractRepository {

	public ActivityRepository() {
		super(ActivityEntity.class);
	}

}
