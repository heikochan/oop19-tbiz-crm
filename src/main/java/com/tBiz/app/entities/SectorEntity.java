/**
 * 
 */
package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "sector", uniqueConstraints = { @UniqueConstraint(columnNames = "id"),
		@UniqueConstraint(columnNames = "name") })
public class SectorEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "name", unique = true, nullable = false, length = 50)
	private String name;

	@Column(name = "notes", columnDefinition = "TEXT")
	private String notes;

	/*
	 * contact_type
	 * 
	 * @OneToMany
	 * 
	 * @JoinTable(name = "customer_job_sector", joinColumns = @JoinColumn(name =
	 * "sector_id"), inverseJoinColumns = @JoinColumn(name = "customer_id"))
	 * 
	 * @OnDelete(action = OnDeleteAction.CASCADE) private Set customers;
	 */

	/**
	 * @param name
	 * @param notes
	 */
	public SectorEntity(String name, String notes) {
		super();
		this.name = name;
		this.notes = notes;
	}

	/**
	 * @param name
	 */
	public SectorEntity(int id, String name, String notes) {
		super();
		this.id = id;
		this.name = name;
		this.notes = notes;
	}

	public SectorEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return this.notes;
	}

	/**
	 * @return the customers
	 */
	/*
	 * public Set getCustomers() { return this.customers; }
	 */

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String getDescription() {
		return this.name;
	}

	/**
	 * @param customers the customers to set
	 */
	/*
	 * public void setCustomers(Set customers) { this.customers = customers; }
	 */

	public boolean isValid() {
		return (this.name != "");
	}

	@Override
	public void setDescription(String description) {
		this.setName(description);
	}

}