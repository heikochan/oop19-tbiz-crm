/**
 * 
 */
package com.tBiz.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.mapping.Set;

@Entity
@Table(name = "contact_info", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class ContactInfoEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "representative_name", nullable = false)
	private String name;

	@Column(name = "value")
	private String value;

	@ManyToOne
	@JoinColumn(name = "contact_type_id", nullable = false)
	// @OnDelete(action = OnDeleteAction.CASCADE)
	private ContactTypeEntity contactType;

	@ManyToOne
	@JoinColumn(name = "customer_id", nullable = false)
	// @OnDelete(action = OnDeleteAction.CASCADE)
	private CustomerEntity customer;

	/**
	 * 
	 * @param name
	 * @param value
	 * @param contactType
	 * @param customer
	 */
	public ContactInfoEntity(String name, ContactTypeEntity contactType, Set customers) {
		super();
		this.name = name;
		this.contactType = contactType;
		// this.customers = customers;
	}

	/**
	 * @param id
	 * @param name
	 * @param value
	 * @param contactType
	 * @param customer
	 */
	public ContactInfoEntity(String name, String reference, ContactTypeEntity contactType, CustomerEntity customer) {
		super();
		this.name = name;
		this.value = reference;
		this.contactType = contactType;
		this.customer = customer;
	}

	public ContactInfoEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * @return the contactType
	 */
	public ContactTypeEntity getContactType() {
		return this.contactType;
	}

	/**
	 * @return the customer
	 */

	public CustomerEntity getCustomer() {
		return this.customer;
	}

	/**
	 * @return the contactType name
	 */
	public String getContactTypeName() {
		return this.contactType.getName();
	}

	/**
	 * @return the customer
	 */

	public String getCustomerName() {
		return this.customer.getDescription();
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @param contactType the contactType to set
	 */
	public void setContactType(ContactTypeEntity contactType) {
		this.contactType = contactType;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.name;
	}

	/**
	 * @param customer the customers to set
	 */

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	@Override
	public void setDescription(String description) {
	}

	/**
	 * @param customer the customer to set
	 */
	/*
	 * public void addCustomer(CustomerEntity customer) {
	 * this.customers.add(customer); }
	 */

}