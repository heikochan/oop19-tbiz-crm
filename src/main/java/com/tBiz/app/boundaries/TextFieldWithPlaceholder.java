package com.tBiz.app.boundaries;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import javax.swing.FocusManager;
import javax.swing.JTextField;

/**
 * TextField with a placeholder
 * extends JTextField
 * show a placeholder in text when empty
 *
 */
public class TextFieldWithPlaceholder extends JTextField{
	
	private String placeholder;
	
	@Override
	protected void paintComponent(java.awt.Graphics g) {
	    super.paintComponent(g);
	
	    if(getText().isEmpty() && ! (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == this)){
	        Graphics2D g2 = (Graphics2D)g.create();
	        g2.setBackground(Color.gray);
	        g2.setFont(getFont().deriveFont(Font.ITALIC));
	        g2.drawString(placeholder, 5, 20);
	        g2.dispose();
	    }
	}
	
	/**
	 * Set the placeholder to show
	 * @param placeholder	text for the placeholder
	 */
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
	
	/**
	 * retrieve the placeholder
	 * @return	text of the placeholder
	 */
	public String getPlaceholder() {
		return this.placeholder;
	}
}