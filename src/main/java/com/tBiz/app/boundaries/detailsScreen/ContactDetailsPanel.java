package com.tBiz.app.boundaries.detailsScreen;

import java.awt.Color;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.tBiz.app.controllers.ContactController;
import com.tBiz.app.entities.ContactInfoEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.CustomerEntity;
import com.tBiz.app.entities.repositories.ContactTypeRepository;
import com.tBiz.app.entities.repositories.CustomerRepository;

public class ContactDetailsPanel extends DetailViewPanel {
	private JPanel panel = new JPanel();
	private JTextField txtName = new JTextField();
	private JTextField txtValue = new JTextField();
	private JComboBox<Object[]> cbCType;
	private JComboBox<Object[]> cbCustomer;
	private ContactTypeRepository ctm = new ContactTypeRepository();
	private CustomerRepository cm = new CustomerRepository();
	private DefaultComboBoxModel model = new DefaultComboBoxModel();
	private int selectedCtIndex;
	private int selectedCuIndex;

	private static final long serialVersionUID = 5780502524459484585L;

	public ContactDetailsPanel(ContactController ctrl, ContactInfoEntity contact) {
		super("Contatto", true, true, true, true);

		this.entity = (ContactInfoEntity) contact;
		this.controller = ctrl;

		List<ContactTypeEntity> contactTypeEnt = ctm.findAll(null);
		cbCType = new JComboBox<Object[]>();
		cbCType.setRenderer(new CustomListRenderer());
		cbCType.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i <= contactTypeEnt.size(); i++) {
			int idx=i-1;
			ContactTypeEntity ct = contactTypeEnt.get(idx);
			Object[] itemData = new Object[] { ct.getId(), ct.getName() };
			// cbCType.addItem(itemData);
			cbCType.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && ct.getId() == ((ContactInfoEntity) this.entity).getContactType().getId()) {
				selectedCtIndex = i;
			}
		}

		List<CustomerEntity> customerEnt = cm.findAll(null);
		cbCustomer = new JComboBox<Object[]>();
		cbCustomer.setRenderer(new CustomListRenderer());
		cbCustomer.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i <= customerEnt.size(); i++) {
			int idx = i-1;
			CustomerEntity cus = customerEnt.get(idx);
			Object[] itemData = new Object[] { cus.getId(), cus.getDescription() };
			// cbCustomer.addItem(itemData);
			cbCustomer.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && cus.getId() == ((ContactInfoEntity) this.entity).getCustomer().getId()) {
				selectedCuIndex = i;
			}
		}
		

		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
						FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
						FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, }));

		JLabel lblName = new JLabel("Nome");
		contentPanel.add(lblName, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtName, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtName.setColumns(this.TEXTFIELD_COLUMNS);

		JLabel lblValue = new JLabel("Valore");
		contentPanel.add(lblValue, this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtValue,
				this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtValue.setColumns(this.TEXTFIELD_COLUMNS);

		this.rowStart += 2;

		JLabel lblType = new JLabel("Tipo");
		contentPanel.add(lblType, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cbCType, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		JLabel lblCustomer = new JLabel("Cliente");
		contentPanel.add(lblCustomer, this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cbCustomer,
				this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

	}

	@Override
	public void drawDetails() {
		ContactInfoEntity contact = (ContactInfoEntity) this.entity;
		// System.out.println("panel:" + contact.getName());
		txtName.setText(contact.getName());
		txtValue.setText(contact.getValue());
		cbCType.setSelectedIndex(this.selectedCtIndex);
		cbCustomer.setSelectedIndex(this.selectedCuIndex);
		panel.repaint();
	}

	@Override
	public void saveEntity() {
		ContactInfoEntity contact = (ContactInfoEntity) this.entity;
		Object[] ctSelection = (Object[]) cbCType.getSelectedObjects()[0];
		System.out.println(ctSelection[0]);
		if ((int) ctSelection[0] != 0) {
			ContactTypeEntity ctEnt = (ContactTypeEntity) ctm.find((int) ctSelection[0]);
			contact.setContactType(ctEnt);
		}
		Object[] cuSelection = (Object[]) cbCustomer.getSelectedObjects()[0];
		if ((int) cuSelection[0] != 0) {
			CustomerEntity cuEnt = (CustomerEntity) cm.find((int) cuSelection[0]);
			contact.setCustomer(cuEnt);
		}
		contact.setName(txtName.getText());
		contact.setValue(txtValue.getText());

		this.controller.saveDetails(contact);

	}
}
