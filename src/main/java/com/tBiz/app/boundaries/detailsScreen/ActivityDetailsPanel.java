package com.tBiz.app.boundaries.detailsScreen;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.text.DateFormatter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.tBiz.app.boundaries.DateLabelFormatter;
import com.tBiz.app.controllers.ActivityController;
import com.tBiz.app.entities.ActivityEntity;
import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.ContactInfoEntity;
import com.tBiz.app.entities.CustomerEntity;
import com.tBiz.app.entities.repositories.ActivityTypeRepository;
import com.tBiz.app.entities.repositories.ContactRepository;
import com.tBiz.app.entities.repositories.CustomerRepository;

public class ActivityDetailsPanel extends DetailViewPanel {

	private JPanel panel = new JPanel();
	private JTextField txtTitle = new JTextField();
	private JTextArea txtDescription = new JTextArea(5, 20);
	private JTextArea txtNote = new JTextArea(5, 20);
	private JDatePickerImpl dtStartDate = null;
	private JSpinner spnStartTime = null;
	private JSpinner spnEndTime = null;
	private JDatePickerImpl dtEndDate = null;
	private int selectedAtIndex = 0;
	private int selectedCuIndex = 0;
	private int selectedCtIndex = 0;

	private ActivityTypeRepository atm = new ActivityTypeRepository();
	private JComboBox cboActivityType = new JComboBox();
	private CustomerRepository cum = new CustomerRepository();
	private JComboBox cboCustomer = new JComboBox();
	private ContactRepository ctm = new ContactRepository();
	private JComboBox cboContact = new JComboBox();

	private static final long serialVersionUID = 5780502524459484585L;

	public ActivityDetailsPanel(ActivityController ctrl, ActivityEntity address) {
		super("Attività", true, true, true, true);

		this.entity = (ActivityEntity) address;
		this.controller = ctrl;

		List<ActivityTypeEntity> ae = this.atm.findAll(null);
		cboActivityType = new JComboBox<Object[]>();
		cboActivityType.setRenderer(new CustomListRenderer());
		cboActivityType.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i < ae.size(); i++) {
			int idx = i - 1;
			ActivityTypeEntity ct = ae.get(idx);
			Object[] itemData = new Object[] { ct.getId(), ct.getDescription() };
			// cbCType.addItem(itemData);
			cboActivityType.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && ct.getId() == ((ActivityEntity) this.entity).getActivityType().getId()) {
				selectedAtIndex = i;
			}
		}

		List<ContactInfoEntity> cie = this.ctm.findAll(null);
		cboContact = new JComboBox<Object[]>();
		cboContact.setRenderer(new CustomListRenderer());
		cboContact.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i < cie.size(); i++) {
			int idx = i - 1;
			ContactInfoEntity ct = cie.get(idx);
			Object[] itemData = new Object[] { ct.getId(), ct.getDescription() };
			// cbCType.addItem(itemData);
			cboContact.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && ct.getId() == ((ActivityEntity) this.entity).getContact().getId()) {
				this.selectedCtIndex = i;
			}
		}

		List<CustomerEntity> cue = this.cum.findAll(null);
		cboCustomer = new JComboBox<Object[]>();
		cboCustomer.setRenderer(new CustomListRenderer());
		cboCustomer.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i < cue.size(); i++) {
			int idx = i - 1;
			CustomerEntity cut = cue.get(idx);
			Object[] itemData = new Object[] { cut.getId(), cut.getDescription() };
			// cbCType.addItem(itemData);
			cboCustomer.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && cut.getId() == ((ActivityEntity) this.entity).getCustomer().getId()) {
				this.selectedCuIndex = i;
			}
		}
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
						FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
						FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
						FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
						FormSpecs.DEFAULT_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, }));

		Properties p = new Properties();
		p.put("text.today", "Oggi");
		p.put("text.month", "Mese");
		p.put("text.year", "Anno");

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 24);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		SpinnerDateModel modelStartTime = new SpinnerDateModel();
		modelStartTime.setValue(calendar.getTime());

		this.spnStartTime = new JSpinner(modelStartTime);
		JSpinner.DateEditor editor = new JSpinner.DateEditor(this.spnStartTime, "kk:mm");
		DateFormatter formatter = (DateFormatter) editor.getTextField().getFormatter();
		formatter.setAllowsInvalid(false);
		formatter.setOverwriteMode(true);
		this.spnStartTime.setEditor(editor);

		UtilDateModel startDtModel = new UtilDateModel();
		JDatePanelImpl startDtPanel = new JDatePanelImpl(startDtModel, p);
		dtStartDate = new JDatePickerImpl(startDtPanel, new DateLabelFormatter());

		JLabel lblStartDate = new JLabel("Inizio Attività");
		contentPanel.add(lblStartDate, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.dtStartDate,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		contentPanel.add(this.spnStartTime,
				this.label_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		SpinnerDateModel modelEndTime = new SpinnerDateModel();
		modelEndTime.setValue(calendar.getTime());

		this.spnEndTime = new JSpinner(modelEndTime);
		editor = new JSpinner.DateEditor(this.spnEndTime, "kk:mm");
		formatter = (DateFormatter) editor.getTextField().getFormatter();
		formatter.setAllowsInvalid(false);
		formatter.setOverwriteMode(true);
		this.spnEndTime.setEditor(editor);

		UtilDateModel endDtModel = new UtilDateModel();
		JDatePanelImpl endDtPanel = new JDatePanelImpl(endDtModel, p);
		dtEndDate = new JDatePickerImpl(endDtPanel, new DateLabelFormatter());

		JLabel lblEndDate = new JLabel("Fine Attività");
		contentPanel.add(lblEndDate, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.dtEndDate,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		contentPanel.add(this.spnEndTime,
				this.label_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		JLabel lblType = new JLabel("Attività");
		contentPanel.add(lblType, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cboActivityType,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		JLabel lblTitle = new JLabel("Titolo");
		contentPanel.add(lblTitle, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtTitle,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtTitle.setColumns(this.TEXTFIELD_COLUMNS);

		this.rowStart += 2;

		JLabel lblDescription = new JLabel("Descrizione");
		contentPanel.add(lblDescription,
				this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtDescription,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		JLabel lblCustomer = new JLabel("Cliente/Lead");
		contentPanel.add(lblCustomer, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cboCustomer,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		JLabel lblContact = new JLabel("Contatto");
		contentPanel.add(lblContact, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cboContact,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		JLabel lblNote = new JLabel("Note");
		contentPanel.add(lblNote, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtNote, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtNote.setColumns(10);
	}

	@Override
	public void drawDetails() {
		ActivityEntity activity = (ActivityEntity) this.entity;
		if (activity.getStartDate() != null) {
			LocalDateTime dt = activity.getStartDate();
			this.dtStartDate.getModel().setDate(dt.getYear(), dt.getMonthValue() - 1, dt.getDayOfMonth());
			this.dtStartDate.getModel().setSelected(true);
			this.spnStartTime.getModel().setValue(dt);
		}
		if (activity.getEndDate() != null) {
			LocalDateTime dt = activity.getEndDate();
			this.dtEndDate.getModel().setDate(dt.getYear(), dt.getMonthValue() - 1, dt.getDayOfMonth());
			this.dtEndDate.getModel().setSelected(true);
			this.spnEndTime.getModel().setValue(dt);
		}

		txtTitle.setText(activity.getTitle());
		txtDescription.setText(activity.getDescription());
		cboActivityType.setSelectedItem(selectedAtIndex);
		cboCustomer.setSelectedItem(selectedCuIndex);
		cboContact.setSelectedItem(selectedCtIndex);
		panel.repaint();
	}

	@Override
	public void saveEntity() {
		ActivityEntity activity = (ActivityEntity) this.entity;
		// Recupero l'indce dell'opzione scelta per il tipo di attività, cerco l'oggetto
		// corrispondente nel db e lo associo all'entità attività
		System.out.println(cboActivityType.getSelectedObjects());
		if (cboActivityType.getSelectedObjects().length > 0) {
			Object[] atSelection = (Object[]) cboActivityType.getSelectedObjects()[0];
			if ((int) atSelection[0] != 0) {
				ActivityTypeEntity atEnt = (ActivityTypeEntity) atm.find((int) atSelection[0]);
				activity.setActivityType(atEnt);
			}
		} else {
			activity.setActivityType(null);
		}

		// Recupero l'indice dell'opzione scelta per il cliente/lead, cerco l'oggetto
		// corrispondente nel db e lo associo all'entità attività
		if (cboCustomer.getSelectedObjects().length > 0) {
			Object[] cuSelection = (Object[]) cboCustomer.getSelectedObjects()[0];
			if ((int) cuSelection[0] != 0) {
				CustomerEntity cuEnt = (CustomerEntity) cum.find((int) cuSelection[0]);
				activity.setCustomer(cuEnt);
			}
		} else {
			activity.setCustomer(null);
		}
		// Recupero l'indice dell'opzione scelta per il contatto, cerco l'oggetto
		// corrispondente nel db e lo associo all'entità attività
		if (cboCustomer.getSelectedObjects().length > 0) {
			Object[] conSelection = (Object[]) cboContact.getSelectedObjects()[0];
			if ((int) conSelection[0] != 0) {
				ContactInfoEntity conEnt = (ContactInfoEntity) ctm.find((int) conSelection[0]);
				activity.setContact(conEnt);
			}
		} else {
			activity.setContact(null);
		}
		activity.setTitle(txtTitle.getText());
		activity.setDescription(txtDescription.getText());
		activity.setNotes(txtNote.getText());

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm");
		SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");
		if (dtStartDate.getJFormattedTextField() != null && !spnStartTime.getValue().equals(null)) {
			LocalDate ldStart = DateLabelFormatter.formatToLocalDate(dtStartDate.getJFormattedTextField().getText(),
					"dd-MM-yyyy");
			String startDate = ldStart.toString() + " " + sdf.format(spnStartTime.getValue());
			LocalDateTime ldtStart = LocalDateTime.parse(startDate, formatter);
			activity.setStartDate(ldtStart);
		} else {
			activity.setStartDate(null);
		}

		if (dtEndDate.getJFormattedTextField() != null) {
			LocalDate ldEnd = DateLabelFormatter.formatToLocalDate(dtEndDate.getJFormattedTextField().getText(),
					"dd-MM-yyyy");
			String endDate = ldEnd.toString() + " " + sdf.format(spnEndTime.getValue());
			LocalDateTime ldtEnd = LocalDateTime.parse(endDate, formatter);
			activity.setEndDate(ldtEnd);
		} else {
			activity.setEndDate(null);
		}

		this.controller.saveDetails(activity);

	}
}
