package com.tBiz.app.boundaries.detailsScreen;

import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;

interface DetailViewInterface {

	abstract void drawDetails();

	void refreshTitle();

	abstract void saveEntity();

	void resetEntity();

	boolean isValidDate(String dateString);

	Date convertStringToDate(String dateString);

	void addToolbarButtons(HashMap<String, ActionListener> buttons);
}
