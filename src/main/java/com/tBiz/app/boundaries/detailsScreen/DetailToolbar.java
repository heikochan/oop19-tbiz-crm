package com.tBiz.app.boundaries.detailsScreen;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.UIManager;

import com.tBiz.app.boundaries.defaultConfigs.DefaultObjectValues;

public class DetailToolbar extends JToolBar {
	JButton btnSave = new JButton("Salva");
	JButton btnUndo = new JButton("Ripristina");
	// JButton btnClose = new JButton("chiudi");

	private Font buttonFont = (Font) DefaultObjectValues.BUTTONS_FONT.getValue();
	private Insets buttonMargin = (Insets) DefaultObjectValues.BUTTONS_MARGIN.getValue();

	public DetailToolbar(DetailViewPanel parent) {
		setBackground(UIManager.getColor("Menu.background"));
		// Configurazione pulsante Salvataggio
		btnSave.setFont(buttonFont);
		btnSave.setMargin(new Insets(5, 10, 5, 10));

		// Configurazione pulsante reset
		btnUndo.setFont(buttonFont);
		btnUndo.setMargin(buttonMargin);

		// Configurazione pulsante chiusura
		/*
		 * btnClose.setFont(buttonFont); btnClose.setMargin(buttonMargin);
		 */

		add(btnSave);
		add(btnUndo);
		addSeparator();
		// add(btnClose);

		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.saveEntity();
			}
		});

		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.resetEntity();
			}
		});
	}
}
