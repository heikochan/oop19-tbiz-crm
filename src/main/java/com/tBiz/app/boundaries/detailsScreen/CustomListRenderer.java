package com.tBiz.app.boundaries.detailsScreen;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class CustomListRenderer extends JLabel implements ListCellRenderer {

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {

		if (value != null) {
			Object[] item = (Object[]) value;
			setText((String) item[1]);
		}

		/*
		 * if (index == -1) { Object[] item = (Object[]) value; setText("" + (int)
		 * item[0]); }
		 */
		return this;
	}
}