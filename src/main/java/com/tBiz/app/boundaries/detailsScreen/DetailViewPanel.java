package com.tBiz.app.boundaries.detailsScreen;

import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.apache.commons.validator.GenericValidator;

import com.tBiz.app.boundaries.defaultConfigs.DefaultNumberValues;
import com.tBiz.app.boundaries.defaultConfigs.DefaultStringValues;
import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.entities.AbstractEntity;

public abstract class DetailViewPanel<T extends AbstractController, E extends AbstractEntity> extends JInternalFrame
		implements DetailViewInterface {
	private JTextField textField;
	protected T controller;
	protected E entity;
	protected JPanel contentPanel = new JPanel();
	protected JToolBar toolBar = new DetailToolbar(this);
	protected HashMap<String, Object> activeFilters;

	protected final String validDateFormat = "dd/mm/yyyy";
	protected final String entityDateFormat = "yyyy/mm/dd";

	protected final int TEXTFIELD_COLUMNS = DefaultNumberValues.TEXTFIELD_SIZE_COLUMNS.getValue();
	protected final String TEXTFIELD_HPOS = DefaultStringValues.TEXTFIELD_HORIZONTAL_POSITION.getValue();
	protected final String TEXTFIELD_VPOS = DefaultStringValues.TEXTFIELD_VERTICAL_POSITION.getValue();
	protected final int COL_LABEL_1COL_START = DefaultNumberValues.COL_LABEL_LAYOUT1COL_START_POSITION.getValue();
	protected final int COL_FIELD_1COL_START = DefaultNumberValues.COL_FIELD_LAYOUT1COL_START_POSITION.getValue();
	protected final int COL_LABEL_2COL_START = DefaultNumberValues.COL_LABEL_LAYOUT2COL_START_POSITION.getValue();
	protected final int COL_FIELD_2COL_START = DefaultNumberValues.COL_FIELD_LAYOUT2COL_START_POSITION.getValue();
	protected final int COL_LABEL_3COL_START = DefaultNumberValues.COL_LABEL_LAYOUT3COL_START_POSITION.getValue();
	protected final int COL_FIELD_3COL_START = DefaultNumberValues.COL_FIELD_LAYOUT3COL_START_POSITION.getValue();
	protected final int ROW_START = DefaultNumberValues.ROW_START_POSITION.getValue();
	protected final String defaultLabelPosition = DefaultStringValues.LABEL_HORIZONTAL_POSITION.getValue() + ", "
			+ DefaultStringValues.LABEL_VERTICAL_POSITION.getValue();
	protected final String defaultFieldPosition = DefaultStringValues.TEXTFIELD_HORIZONTAL_POSITION.getValue() + ", "
			+ DefaultStringValues.TEXTFIELD_VERTICAL_POSITION.getValue();

	protected int groupContainergap = DefaultNumberValues.GROUP_CONTAINER_GAP.getValue();
	protected int rowStart = this.ROW_START;
	protected int label_col1_start = this.COL_LABEL_1COL_START;
	protected int label_col2_start = this.COL_LABEL_2COL_START;
	protected int field_col1_start = this.COL_FIELD_1COL_START;
	protected int field_col2_start = this.COL_FIELD_2COL_START;
	protected int label_col3_start = this.COL_LABEL_3COL_START;
	protected int field_col3_start = this.COL_FIELD_3COL_START;

	protected String[] booleanOptions = { "No", "Si" };

	public DetailViewPanel(String name, boolean resize, boolean close, boolean maximize, boolean reducetoicon) {
		super(name, resize, close, maximize, reducetoicon);
		getContentPane().setBackground(UIManager.getColor("EditorPane.background"));

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup().addGap(this.groupContainergap)
						.addComponent(contentPanel, GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE).addContainerGap()));
		contentPanel.setBackground(UIManager.getColor("EditorPane.background"));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addGap(this.groupContainergap)
						.addComponent(contentPanel, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(this.groupContainergap, Short.MAX_VALUE)));

		groupLayout.setAutoCreateGaps(true);
		getContentPane().setLayout(groupLayout);

		addInternalFrameListener(new InternalFrameAdapter() {
			public void internalFrameClosing(InternalFrameEvent e) {
				controller.viewList();
			}
		});
	}

	public abstract void drawDetails();

	public void refreshTitle() {
		if (this.entity.getDescription() == null)
			this.title = "Nuovo" + " " + this.title;
		else
			this.title = this.title + ":" + this.entity.getDescription();
	}

	public abstract void saveEntity();

	public void resetEntity() {
		this.drawDetails();
	}

	public boolean isValidDate(String dateString) {
		System.out.println("Valore data:" + dateString);
		if (!dateString.equals("")) {
			boolean isValid = GenericValidator.isDate(dateString, validDateFormat, true);
			if (!isValid) {
				Icon errorIcon = UIManager.getIcon("OptionPane.errorIcon");
				String msg = "Il formato della data è sbagliato. Inserire una data nel dormato GG/MM/AAAA";
				JOptionPane.showMessageDialog(null, msg, "Formato data non valido", 0, errorIcon);
				return false;
			}
			return true;
		} else {
			return false;
		}
	}

	public Date convertStringToDate(String dateString) {
		try {
			Date dateToSave = new SimpleDateFormat(entityDateFormat).parse(dateString);
			return dateToSave;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void addToolbarButtons(HashMap<String, ActionListener> buttons) {
		int i = 0;
		Set<String> keys = buttons.keySet();
		for (String key : keys) {
			JButton btn = new JButton(key);
			btn.addActionListener(buttons.get(key));
			toolBar.add(btn);
		}
	}

}