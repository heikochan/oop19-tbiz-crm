package com.tBiz.app.boundaries.detailsScreen;

import java.awt.Color;
import java.text.ParseException;
import java.util.List;


import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.tBiz.app.controllers.AddressController;
import com.tBiz.app.entities.AddressEntity;
import com.tBiz.app.entities.CityEntity;
import com.tBiz.app.entities.ContactInfoEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.CustomerEntity;
import com.tBiz.app.entities.repositories.CityRepository;
import com.tBiz.app.entities.repositories.CustomerRepository;

public class AddressDetailsPanel extends DetailViewPanel{

	private JPanel panel = new JPanel();
	private JTextField txtStreet = new JTextField();
	private JFormattedTextField txtPostalCode = new JFormattedTextField();
	private JComboBox<Object[]> cboCities = new JComboBox<Object[]>();
	private int selectedCityIndex;

	private static final long serialVersionUID = 5780502524459484585L;

	public AddressDetailsPanel(AddressController ctrl, AddressEntity address) {
		super("Indirizzo", true, true, true, true);

		this.entity = (AddressEntity) address;
		
		CityRepository cm = new CityRepository();
		List<CityEntity> ce = cm.findAll(null);
		cboCities = new JComboBox<Object[]>();
		cboCities.setRenderer(new CustomListRenderer());
		cboCities.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i < ce.size(); i++) {
			CityEntity ct = ce.get(i);
			Object[] itemData = new Object[] { ct.getId(), ct.getDescription() };
			// cbCType.addItem(itemData);
			cboCities.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && ct.getId() == ((AddressEntity) this.entity).getCity().getId()) {
				selectedCityIndex = i;
			}
		}
		
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
						FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
						FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, }));

		JLabel lblAddress = new JLabel("Indirizzo");
		contentPanel.add(lblAddress, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtStreet, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtStreet.setColumns(this.TEXTFIELD_COLUMNS);
		
		this.rowStart += 2;
		MaskFormatter mask=null;
		try {
			mask = new MaskFormatter("#####");
		}catch (ParseException e) {
		}
		
		txtPostalCode = new JFormattedTextField(mask);
		
		JLabel lblCAP = new JLabel("CAP");
		contentPanel.add(lblCAP, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtPostalCode, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtPostalCode.setColumns(this.TEXTFIELD_COLUMNS);	


		JLabel lblCity = new JLabel("Città");
		contentPanel.add(lblCity, this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cboCities, this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		
		this.controller = ctrl;
		this.activeFilters = ctrl.getFilters();
		
		this.refreshTitle();
	}
	
	@Override
	public void drawDetails() {
		AddressEntity address = (AddressEntity) this.entity;
		txtStreet.setText(address.getStreet());
		txtPostalCode.setText(address.getPostal_code());		
		cboCities.setSelectedItem(this.selectedCityIndex);
		panel.repaint();
	}

	@Override
	public void saveEntity() {
		AddressEntity address = (AddressEntity) this.entity;
		address.setStreet(txtStreet.getText());
		address.setPostal_code(txtPostalCode.getText());
		Object[] city = (Object[]) cboCities.getSelectedObjects()[0];
		if ((int) city[0] > 0) {
			CityRepository cm = new CityRepository();
			CityEntity cc = (CityEntity) cm.find((int) city[0]);
			if (cc != null)
				address.setCity(cc);
		}
		int idCustomer = (int) this.activeFilters.get("customer");
		if( idCustomer > 0 ) {
			CustomerRepository cu = new CustomerRepository();
			CustomerEntity ce = (CustomerEntity) cu.find(idCustomer);
			if(ce!=null)
				address.setCustomer(ce);
		}
		
		this.controller.saveDetails(address);
	}
}
