package com.tBiz.app.boundaries.detailsScreen;

import java.time.LocalDate;
import java.util.List;
import java.util.Properties;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.tBiz.app.boundaries.DateLabelFormatter;
import com.tBiz.app.controllers.CustomerController;
import com.tBiz.app.entities.CustomerEntity;
import com.tBiz.app.entities.SectorEntity;
import com.tBiz.app.entities.repositories.SectorRepository;

public class CustomerDetailsPanel extends DetailViewPanel {

	private static final long serialVersionUID = 5780502524459484585L;

	private JTextField txtCorporate = new JTextField();
	private JTextField txtLastname = new JTextField();
	private JTextField txtName = new JTextField();
	private JTextField txtFiscalCode = new JTextField();
	private JTextField txtVatNumber = new JTextField();
	private JDatePickerImpl dtFirstContactDate = null;
	private JDatePickerImpl dtAcquisitionDate = null;
	private JComboBox isCustomer = new JComboBox(this.booleanOptions);
	private JComboBox<Object[]> cbSector;
	private SectorRepository sm = new SectorRepository();
	private DefaultComboBoxModel model = new DefaultComboBoxModel();
	private int selectedSectIndex;

	private int rowStart = this.ROW_START;
	private int label_col1_start = this.COL_LABEL_1COL_START;
	private int label_col2_start = this.COL_LABEL_2COL_START;
	private int field_col1_start = this.COL_FIELD_1COL_START;
	private int field_col2_start = this.COL_FIELD_2COL_START;
	private int label_col3_start = this.COL_LABEL_3COL_START;
	private int field_col3_start = this.COL_FIELD_3COL_START;

	public CustomerDetailsPanel(CustomerController ctrl, CustomerEntity customer) {
		super("Cliente/Lead", true, true, true, true);
		this.entity = (CustomerEntity) customer;
		this.controller = ctrl;

		txtVatNumber.setColumns(10);
		contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
						FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
						FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, }));

		List<SectorEntity> sectorEnt = sm.findAll(null);
		cbSector = new JComboBox<Object[]>();
		cbSector.setRenderer(new CustomListRenderer());
		cbSector.insertItemAt(new Object[] { 0, "" }, 0);
		for (int i = 1; i < sectorEnt.size(); i++) {
			SectorEntity sc = sectorEnt.get(i);
			System.out.println(sc.getId());
			Object[] itemData = new Object[] { sc.getId(), sc.getName() };
			// cbCType.addItem(itemData);
			cbSector.insertItemAt(itemData, i);
			if (this.entity.getId() > 0 && ((CustomerEntity) this.entity).getSector() != null
					&& sc.getId() == ((CustomerEntity) this.entity).getSector().getId()) {
				selectedSectIndex = i;
			}
		}

		JLabel lblName = new JLabel("Nome");
		contentPanel.add(lblName, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtName, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtName.setColumns(this.TEXTFIELD_COLUMNS);

		JLabel lblLastname = new JLabel("Cognome");
		contentPanel.add(lblLastname, this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtLastname,
				this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtLastname.setColumns(this.TEXTFIELD_COLUMNS);

		this.rowStart += 2;

		JLabel lblFiscalCode = new JLabel("Cod. Fiscale");
		contentPanel.add(lblFiscalCode,
				this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtFiscalCode,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtFiscalCode.setColumns(this.TEXTFIELD_COLUMNS);

		this.rowStart += 2;

		JLabel lblCorporateName = new JLabel("Ragione Sociale");
		contentPanel.add(lblCorporateName,
				this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtCorporate,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtCorporate.setColumns(this.TEXTFIELD_COLUMNS);

		JLabel lblVatNumber = new JLabel("P. IVA");
		contentPanel.add(lblVatNumber, this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtVatNumber,
				this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtVatNumber.setColumns(this.TEXTFIELD_COLUMNS);

		this.rowStart += 2;

		Properties p = new Properties();
		p.put("text.today", "Oggi");
		p.put("text.month", "Mese");
		p.put("text.year", "Anno");

		UtilDateModel firstDtModel = new UtilDateModel();
		JDatePanelImpl firstDtPanel = new JDatePanelImpl(firstDtModel, p);
		dtFirstContactDate = new JDatePickerImpl(firstDtPanel, new DateLabelFormatter());

		JLabel lblFirstContact = new JLabel("Primo Contatto");
		contentPanel.add(lblFirstContact,
				this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.dtFirstContactDate,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		UtilDateModel acqDtModel = new UtilDateModel();
		JDatePanelImpl acqDtPanel = new JDatePanelImpl(acqDtModel, p);
		dtAcquisitionDate = new JDatePickerImpl(acqDtPanel, new DateLabelFormatter());

		JLabel lblAcquisition = new JLabel("Acquisizione");
		contentPanel.add(lblAcquisition,
				this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.dtAcquisitionDate,
				this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		this.rowStart += 2;

		JLabel lblSector = new JLabel("Settore");
		contentPanel.add(lblSector, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.cbSector,
				this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		JLabel lblIscustomer = new JLabel("Cliente?");
		contentPanel.add(lblIscustomer,
				this.label_col2_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.isCustomer,
				this.field_col2_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);

		// this.isCustomer.addActionListener(this);
		/*
		 * contentPanel.add(this.checkIsCustomer, this.label_col1_start + ", " +
		 * this.rowStart + ", " + this.defaultLabelPosition);
		 */

	}

	@Override
	public void drawDetails() {
		CustomerEntity cust = (CustomerEntity) this.entity;
		this.txtCorporate.setText(cust.getCorporateName());
		this.txtLastname.setText(cust.getLastname());
		this.txtName.setText(cust.getFirstname());
		this.txtFiscalCode.setText(cust.getFiscalCode());
		this.txtVatNumber.setText(cust.getVatNumber());
		if (cust.getAcquisitionDate() != null) {
			LocalDate dt = cust.getAcquisitionDate();
			this.dtAcquisitionDate.getModel().setDate(dt.getYear(), dt.getMonthValue() - 1, dt.getDayOfMonth());
			this.dtAcquisitionDate.getModel().setSelected(true);
		}
		if (cust.getFirstContactDate() != null) {
			LocalDate dt = cust.getFirstContactDate();
			this.dtFirstContactDate.getModel().setDate(dt.getYear(), dt.getMonthValue() - 1, dt.getDayOfMonth());
			this.dtFirstContactDate.getModel().setSelected(true);
		}
		this.isCustomer.setSelectedIndex(cust.isAcquired() ? 1 : 0);
		this.cbSector.setSelectedIndex(this.selectedSectIndex);
		// this.isCustomer.setText(cust.getVatNumber());
		this.contentPanel.repaint();
	}

	@Override
	public void saveEntity() {
		CustomerEntity cust = (CustomerEntity) this.entity;
		cust.setFirstname(this.txtName.getText());
		cust.setLastname(this.txtLastname.getText());
		cust.setFiscalCode(this.txtFiscalCode.getText());
		cust.setCorporateName(this.txtCorporate.getText());
		cust.setVatNumber(this.txtVatNumber.getText());
		cust.setAcquired(this.isCustomer.getSelectedIndex() == 1 ? true : false);

		Object[] scSelection = (Object[]) cbSector.getSelectedObjects()[0];
		if ((int) scSelection[0] != 0) {
			SectorEntity scEnt = (SectorEntity) sm.find((int) scSelection[0]);
			cust.setSector(scEnt);
		}

		LocalDate ld = DateLabelFormatter.formatToLocalDate(dtFirstContactDate.getJFormattedTextField().getText(),
				"dd-MM-yyyy");
		cust.setFirstContactDate(ld);
		
		System.out.println(dtAcquisitionDate.getJFormattedTextField().getText());

		ld = DateLabelFormatter.formatToLocalDate(dtAcquisitionDate.getJFormattedTextField().getText(), "dd-MM-yyyy");
		cust.setAcquisitionDate(ld);

		this.controller.saveDetails(cust);
	}

}
