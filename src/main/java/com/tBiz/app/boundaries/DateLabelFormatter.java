package com.tBiz.app.boundaries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 * LocalDate and LocalDateTime manager manages date format between database
 * format and UI format
 *
 */
public class DateLabelFormatter extends AbstractFormatter {

	private String datePattern = "dd-MM-yyyy";
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

	/**
	 * parse a formatted string into a date object
	 * 
	 * @args text string containing a date formatted
	 * @return Object
	 * @throws ParseException
	 */
	public Object stringToValue(String text) throws ParseException {
		return dateFormatter.parseObject(text);
	}

	@Override
	/**
	 * return a date formatted string
	 * 
	 * @args value Date to format
	 * @return String string formatted
	 */
	public String valueToString(Object value) throws ParseException {
		if (value != null) {
			Calendar cal = (Calendar) value;
			return dateFormatter.format(cal.getTime());
		}

		return "";
	}

	/**
	 * returns a LocalDate from a DateString and relative pattern
	 * 
	 * @param dateString string containing the formatted date
	 * @param pattern    pattern of the format
	 * @return LocalDate
	 */
	public static final LocalDate formatToLocalDate(String dateString, String pattern) {
		// if param is null or empty
		if (dateString == null || dateString.length() == 0) {
			// return null
			return null;
		}
		// otherwise
		// in case of LocalDateTime, retrive the first 10 positions
		dateString = dateString.substring(0, 10);
		// instantiate the formatter with the pattern
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		// parse the string
		LocalDate localDate = LocalDate.parse(dateString, formatter);
		return localDate;
	}

	/**
	 * return a formatted String by the pattern param from the input LocalDate
	 * 
	 * @param date    LocalDate to convert
	 * @param pattern format of the result
	 * @return String formatted string
	 */
	public static final String formatDate(LocalDate date, String pattern) {
		String dateString = new String();
		// if param is null
		if (date == null || date.equals(null)) {
			// return empty string
			return "";
		}
		// otherwise
		// instantiate the formatter with the pattern
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		// format the date and get the string
		dateString = date.format(formatter);

		return dateString;
	}

	/**
	 * return a formatted String by the pattern param from the input LocalDateTime
	 * 
	 * @param date    LocalDateTime to convert
	 * @param pattern format of the result
	 * @return String formatted string
	 */
	public static final String formatDateTime(LocalDateTime date, String pattern) {
		String dateString = new String();
		// if param is null
		if (date == null || date.equals(null)) {
			// return empty string
			return "";
		}
		// otherwise
		// instantiate the formatter with the pattern
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		// format the date and get the string
		dateString = date.format(formatter);

		return dateString;
	}
}
