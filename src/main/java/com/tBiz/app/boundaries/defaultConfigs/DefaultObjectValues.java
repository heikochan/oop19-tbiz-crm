package com.tBiz.app.boundaries.defaultConfigs;

import java.awt.Font;
import java.awt.Insets;

public enum DefaultObjectValues {
	DEFAULT_FONT(new Font("Dialog", Font.PLAIN, 14)), BUTTONS_FONT(new Font("Dialog", Font.BOLD, 14)),
	BUTTONS_MARGIN(new Insets(5, 10, 5, 10));

	private Object value;

	private DefaultObjectValues(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return this.value;
	}

}
