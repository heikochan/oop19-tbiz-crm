package com.tBiz.app.boundaries.defaultConfigs;

public enum DefaultStringValues {
	LABEL_VERTICAL_POSITION("center"), LABEL_HORIZONTAL_POSITION("right"), TEXTFIELD_VERTICAL_POSITION("center"),
	TEXTFIELD_HORIZONTAL_POSITION("fill");

	private String value;

	private DefaultStringValues(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
