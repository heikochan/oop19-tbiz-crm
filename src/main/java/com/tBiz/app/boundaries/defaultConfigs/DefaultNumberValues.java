package com.tBiz.app.boundaries.defaultConfigs;

public enum DefaultNumberValues {
	TEXTFIELD_SIZE_COLUMNS(20), ROW_START_POSITION(2), COL_LABEL_LAYOUT1COL_START_POSITION(2),
	COL_FIELD_LAYOUT1COL_START_POSITION(4), COL_LABEL_LAYOUT2COL_START_POSITION(6),
	COL_FIELD_LAYOUT2COL_START_POSITION(8), COL_LABEL_LAYOUT3COL_START_POSITION(10), 
	COL_FIELD_LAYOUT3COL_START_POSITION(12), HEADINGS_FONT_SIZE(20), CONTENT_FONT_SIZE(16), GROUP_CONTAINER_GAP(15);

	private int value;

	private DefaultNumberValues(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

}
