package com.tBiz.app.boundaries.views;

import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.entities.AbstractEntity;

public abstract class AbstractView<T extends AbstractEntity, C extends AbstractController> {

	protected static LinkedHashMap<String, String> fieldsList;
	protected static HashMap<String, String> methodsList;
	protected static HashMap<String, Method> mappedFieldsMethods;
	protected static HashMap<String, ActionListener> panelButtons;
	public static boolean dateFilter;
	protected String viewTitle;
	protected Class<T> entityClass;
	protected C controller;
	protected T entity;

	public AbstractView(Class<T> entClass, String title) {
		fieldsList = new LinkedHashMap<String, String>();
		methodsList = new HashMap<String, String>();
		this.entityClass = entClass;
		panelButtons = new HashMap<String, ActionListener>();
		this.viewTitle = title;
		dateFilter = false;
	}

	public abstract void showList(List<T> entityList);

	public abstract void showDetails(T entity);

	/**
	 * Return a n-dimensional array containing the input entity's information
	 * previously inserted in <i>fieldsList</i> attribute.
	 * @param entityList	EntityList from which retrieve data
	 * @return				n-dimensional array
	 */
	protected Object[][] getTableData(List<T> entityList) {
		// define the n-dimensional array
		// rows = entityList.size
		// columns = fieldsList.size
		Object[][] data = new Object[entityList.size()][fieldsList.size()];
		// for each element in List
		for (int i = 0; i < entityList.size(); i++) {
			List<Object> element = new ArrayList<Object>(fieldsList.size());
			AbstractEntity entity = entityList.get(i);
			Set<String> fieldsKeys = fieldsList.keySet();
			// for each column in fieldsList
			for (String prop : fieldsKeys) {
				Method mth;
				Object propValue = null;
				try {
					// retrieve the property method
					mth = this.entityClass.getMethod(methodsList.get(prop));
					// exec the method to retrieve value
					propValue = mth.invoke(entity);
				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				} catch (SecurityException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
				// add value to the element
				element.add(propValue);
			}
			// insert in final array
			data[i] = element.toArray();
		}
		return data;
	}

	protected abstract void init();
}
