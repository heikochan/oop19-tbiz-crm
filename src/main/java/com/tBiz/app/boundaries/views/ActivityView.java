package com.tBiz.app.boundaries.views;

import java.util.List;

import com.tBiz.app.App;
import com.tBiz.app.boundaries.detailsScreen.ActivityDetailsPanel;
import com.tBiz.app.boundaries.listScreen.ListViewPanel;
import com.tBiz.app.controllers.ActivityController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ActivityEntity;

public class ActivityView extends AbstractView {

	private static final Class<ActivityEntity> ACTIVITY_CLASS = ActivityEntity.class;

	public ActivityView(ActivityController ctrl) {
		super(ACTIVITY_CLASS, "Lista Attività");
		this.controller = ctrl;
		this.init();
	}

	public ActivityView(ActivityController ctrl, String title) {
		super(ACTIVITY_CLASS, title);
		this.controller = ctrl;
		this.init();
	}

	@Override
	public void showList(List activities) {
		this.init();
		ListViewPanel panel = (ListViewPanel) App.getMainFrame().getAlreadyOpen(this.viewTitle);
		if (panel == null) {
			panel = new ListViewPanel(this.controller, this.viewTitle, true, true, true, true);
			App.getMainFrame().getDesktop().add(panel);
			panel.setVisible(true);
			panel.show();
		}
		Object[][] tableData = this.getTableData(activities);
		panel.drawList(tableData, fieldsList.values().toArray());
		if (this.dateFilter) {
			panel.activateDateFilter();
		}

		App.getMainFrame().getDesktop().updateUI();

	}

	@Override
	public void showDetails(AbstractEntity entity) {
		String title = "Indirizzo";
		ActivityDetailsPanel panel = (ActivityDetailsPanel) App.getMainFrame().getAlreadyOpen(title);
		if (panel == null) {
			panel = new ActivityDetailsPanel((ActivityController) this.controller, (ActivityEntity) entity);
			App.getMainFrame().getDesktop().add(panel);
			panel.addToolbarButtons(panelButtons);
			panel.setVisible(true);
			panel.show();
		}
		panel.drawDetails();

		App.getMainFrame().getDesktop().updateUI();
	}

	@Override
	protected void init() {
		fieldsList.clear();
		fieldsList.put("id", "Codice");
		fieldsList.put("title", "Titolo");
		fieldsList.put("description", "Descrizione");
		fieldsList.put("startDate", "Data inizio");
		fieldsList.put("endDate", "Data fine");
		fieldsList.put("activityType", "Tipo attività");
		fieldsList.put("contact", "Contatto");
		fieldsList.put("customer", "Cliente");
		methodsList.clear();
		methodsList.put("id", "getId");
		methodsList.put("title", "getTitle");
		methodsList.put("description", "getDescription");
		methodsList.put("startDate", "getStartDateFormatted");
		methodsList.put("endDate", "getEndDateFormatted");
		methodsList.put("activityType", "getActivityTypeName");
		methodsList.put("contact", "getContactName");
		methodsList.put("customer", "getCustomerName");
	}

}
