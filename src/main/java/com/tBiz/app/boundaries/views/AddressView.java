package com.tBiz.app.boundaries.views;

import java.util.List;

import com.tBiz.app.App;
import com.tBiz.app.boundaries.detailsScreen.AddressDetailsPanel;
import com.tBiz.app.boundaries.listScreen.ListViewPanel;
import com.tBiz.app.controllers.AddressController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.AddressEntity;

public class AddressView extends AbstractView {

	private static final Class<AddressEntity> ACTIVITY_CLASS = AddressEntity.class;

	public AddressView(AddressController ctrl) {
		super(ACTIVITY_CLASS, "Indirizzi");
		this.controller = ctrl;
		this.init();
	}

	public AddressView(AddressController ctrl, String title) {
		super(ACTIVITY_CLASS, title);
		this.controller = ctrl;
		this.init();
	}

	@Override
	public void showList(List contacts) {
		this.init();
		ListViewPanel panel = (ListViewPanel) App.getMainFrame().getAlreadyOpen(viewTitle);
		if (panel == null) {
			panel = new ListViewPanel(this.controller, viewTitle, true, true, true, true);
			App.getMainFrame().getDesktop().add(panel);
			panel.setVisible(true);
			panel.show();
		}
		Object[][] tableData = this.getTableData(contacts);
		panel.drawList(tableData, fieldsList.values().toArray());

		App.getMainFrame().getDesktop().updateUI();
	}

	@Override
	public void showDetails(AbstractEntity entity) {
		String title = "Indirizzo";
		AddressDetailsPanel panel = (AddressDetailsPanel) App.getMainFrame().getAlreadyOpen(title);
		if (panel == null) {
			panel = new AddressDetailsPanel((AddressController) this.controller, (AddressEntity) entity);
			App.getMainFrame().getDesktop().add(panel);
			panel.addToolbarButtons(panelButtons);
			panel.setVisible(true);
			panel.show();
		}
		panel.drawDetails();

		App.getMainFrame().getDesktop().updateUI();
	}

	@Override
	protected void init() {
		fieldsList.clear();
		fieldsList.put("id", "Codice");
		fieldsList.put("street", "Indirizzo");
		fieldsList.put("postal_code", "CAP");
		fieldsList.put("city", "Città");
		methodsList.clear();
		methodsList.put("id", "getId");
		methodsList.put("street", "getStreet");
		methodsList.put("postal_code", "getPostal_code");
		methodsList.put("city", "getCityName");
	}

}
