package com.tBiz.app.boundaries.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import com.tBiz.app.App;
import com.tBiz.app.boundaries.detailsScreen.CustomerDetailsPanel;
import com.tBiz.app.boundaries.listScreen.ListViewPanel;
import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.controllers.ActivityController;
import com.tBiz.app.controllers.AddressController;
import com.tBiz.app.controllers.ContactController;
import com.tBiz.app.controllers.CustomerController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.CustomerEntity;

public class CustomerView extends AbstractView {

	private static final Class<CustomerEntity> CUSTOMER_CLASS = CustomerEntity.class;

	public CustomerView(CustomerController ctrl) {
		super(CUSTOMER_CLASS, "Lista Clienti/Leads");
		this.controller = ctrl;
		this.init();
	}

	public CustomerView(CustomerController ctrl, String title) {
		super(CUSTOMER_CLASS, title);
		this.controller = ctrl;
		this.init();

	}

	@Override
	public void showList(List customers) {
		this.init();
		ListViewPanel<AbstractController, AbstractEntity> panel = (ListViewPanel) App.getMainFrame()
				.getAlreadyOpen(viewTitle);
		if (panel == null) {
			panel = new ListViewPanel<AbstractController, AbstractEntity>(this.controller, viewTitle, true, true, true,
					true);
			App.getMainFrame().getDesktop().add(panel);
			panel.setVisible(true);
			panel.show();
		}
		Object[][] tableData = this.getTableData(customers);
		panel.drawList(tableData, fieldsList.values().toArray());

		App.getMainFrame().getDesktop().updateUI();

	}

	@Override
	public void showDetails(AbstractEntity entity) {
		this.panelButtons.put("Indirizzi", new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddressController at = new AddressController();
				at.addFilter("customer", entity.getId());
				at.viewList();
			}
		});
		this.panelButtons.put("Contatti", new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ContactController ct = new ContactController();
				ct.addFilter("customer", entity.getId());
				ct.viewList();
			}
		});
		this.panelButtons.put("Attività", new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ActivityController at = new ActivityController();
				at.addFilter("customer", entity.getId());
				at.viewList();
			}
		});

		String title = "Dettaglio Cliente/Lead";
		CustomerDetailsPanel panel = (CustomerDetailsPanel) App.getMainFrame().getAlreadyOpen(title);
		if (panel == null) {
			panel = new CustomerDetailsPanel((CustomerController) this.controller, (CustomerEntity) entity);
			App.getMainFrame().getDesktop().add(panel);
			panel.addToolbarButtons(this.panelButtons);
			panel.setVisible(true);
			panel.show();
		}
		panel.drawDetails();

		App.getMainFrame().getDesktop().updateUI();
	}

	@Override
	protected void init() {
		fieldsList.clear();
		fieldsList.put("id", "Codice");
		fieldsList.put("lastname", "Cognome");
		fieldsList.put("firstname", "Nome");
		fieldsList.put("corporateName", "Ragione Sociale");
		fieldsList.put("fiscalCode", "Codice Fiscale");
		fieldsList.put("vatNumber", "P. IVA");
		fieldsList.put("firstContactDate", "Primo contatto");
		fieldsList.put("acquisitionDate", "Acquisizione");
		methodsList.clear();
		methodsList.put("id", "getId");
		methodsList.put("lastname", "getLastname");
		methodsList.put("firstname", "getFirstname");
		methodsList.put("corporateName", "getCorporateName");
		methodsList.put("fiscalCode", "getFiscalCode");
		methodsList.put("vatNumber", "getVatNumber");
		methodsList.put("firstContactDate", "getFirstContactDateFormatted");
		methodsList.put("acquisitionDate", "getAcquisitionDateFormatted");

	}

}
