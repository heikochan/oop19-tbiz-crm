package com.tBiz.app.boundaries.views;

import java.util.List;

import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.entities.AbstractEntity;

interface ViewInterface<T extends AbstractEntity, C extends AbstractController> {

	abstract void showList(List<T> entityList);

	abstract void showDetails(T entity);

}
