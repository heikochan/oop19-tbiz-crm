package com.tBiz.app.boundaries.views;

import java.util.List;

import com.tBiz.app.App;
import com.tBiz.app.boundaries.detailsScreen.ContactDetailsPanel;
import com.tBiz.app.boundaries.listScreen.ListViewPanel;
import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.controllers.ContactController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ContactInfoEntity;

public class ContactView extends AbstractView {

	private static final Class<ContactInfoEntity> CONTACT_CLASS = ContactInfoEntity.class;

	public ContactView(ContactController ctrl) {
		super(CONTACT_CLASS, "Contatti");
		this.controller = ctrl;
		this.init();
	}

	public ContactView(ContactController ctrl, String title) {
		super(CONTACT_CLASS, title);
		this.controller = ctrl;
		this.init();
	}

	@Override
	protected void init() {
		fieldsList.clear();
		fieldsList.put("id", "Codice");
		fieldsList.put("name", "Nome Completo");
		fieldsList.put("value", "Valore");
		fieldsList.put("contactType", "Tipologia contatto");
		fieldsList.put("customer", "Cliente di riferimento");
		methodsList.clear();
		methodsList.put("id", "getId");
		methodsList.put("name", "getName");
		methodsList.put("value", "getValue");
		methodsList.put("contactType", "getContactTypeName");
		methodsList.put("customer", "getCustomerName");
	}

	@Override
	public void showList(List contacts) {
		this.init();
		ListViewPanel<AbstractController, AbstractEntity> panel = (ListViewPanel<AbstractController, AbstractEntity>) App
				.getMainFrame().getAlreadyOpen(viewTitle);
		if (panel == null) {
			panel = new ListViewPanel<AbstractController, AbstractEntity>(this.controller, viewTitle, true, true, true,
					true);
			App.getMainFrame().getDesktop().add(panel);
			panel.setVisible(true);
			panel.show();
		}
		Object[][] tableData = this.getTableData(contacts);
		panel.drawList(tableData, fieldsList.values().toArray());

		App.getMainFrame().getDesktop().updateUI();
	}

	@Override
	public void showDetails(AbstractEntity entity) {
		ContactDetailsPanel panel = new ContactDetailsPanel((ContactController) this.controller,
				(ContactInfoEntity) entity);
		panel.drawDetails();
		App.getMainFrame().getDesktop().add(panel);

		panel.setVisible(true);
		panel.show();

		App.getMainFrame().getDesktop().updateUI();
	}

}
