package com.tBiz.app.boundaries.views;

import java.util.List;

import com.tBiz.app.App;
import com.tBiz.app.boundaries.SimpleTableDetailsPanel;
import com.tBiz.app.boundaries.listScreen.ListViewPanel;
import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.controllers.SimpleTablesController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.SectorEntity;

public class SimpleTablesView extends AbstractView {

	private static final Class<AbstractEntity> CONTACT_CLASS = AbstractEntity.class;

	public SimpleTablesView(SimpleTablesController ctrl, String title) {
		super(CONTACT_CLASS, title);
		this.controller = ctrl;
		this.init();

	}

	@Override
	public void showList(List entityList) {
		this.init();
		SimpleTablesController stc = new SimpleTablesController(new ContactTypeEntity());
		if (this.controller.getModel().getEntityClass().isInstance(new ContactTypeEntity())) {
			stc = new SimpleTablesController(new ContactTypeEntity());
		} else if (this.controller.getModel().getEntityClass().isInstance(new ActivityTypeEntity())) {
			stc = new SimpleTablesController(new ActivityTypeEntity());
		} else if (this.controller.getModel().getEntityClass().isInstance(new SectorEntity())) {
			stc = new SimpleTablesController(new SectorEntity());
		}
		ListViewPanel<AbstractController, AbstractEntity> panel = (ListViewPanel<AbstractController, AbstractEntity>) App
				.getMainFrame().getAlreadyOpen(viewTitle);
		if (panel == null) {
			panel = new ListViewPanel<AbstractController, AbstractEntity>(stc, viewTitle, true, true, true, true);
			App.getMainFrame().getDesktop().add(panel);
			panel.setVisible(true);
			panel.show();
		}
		Object[][] tableData = this.getTableData(entityList);
		panel.drawList(tableData, fieldsList.values().toArray());

		panel.setVisible(true);
		panel.show();

		App.getMainFrame().getDesktop().updateUI();
	}

	public void showDetails(AbstractEntity entity) {
		SimpleTableDetailsPanel panel = new SimpleTableDetailsPanel(new SimpleTablesController(entity), entity);
		panel.drawDetails();
		App.getMainFrame().getDesktop().add(panel);

		panel.setVisible(true);
		panel.show();

		App.getMainFrame().getDesktop().updateUI();
	}

	@Override
	protected void init() {
		fieldsList.clear();
		fieldsList.put("id", "Codice");
		fieldsList.put("descrizione", "Descrizione");
		methodsList.clear();
		methodsList.put("id", "getId");
		methodsList.put("descrizione", "getDescription");
	}

}
