package com.tBiz.app.boundaries;

import javax.swing.JTable;

/**
 * The ViewOnlyTable is a JTable with disabled cells 
 * that permitt row selection
 */
public class ViewOnlyTable extends JTable{
	
	public ViewOnlyTable(Object[][] data, Object[] columns) {
		super(data,columns);
	}
	
	public boolean isCellEditable(int row, int column) {
		return false;
	}
	
}
