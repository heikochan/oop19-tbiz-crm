package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.controllers.CustomerController;
import com.tBiz.app.controllers.SimpleTablesController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.SectorEntity;

public class SimpleTablesMenuAction <T extends AbstractEntity> extends AbstractAction {
	
	private T entity;
	
	private static final long serialVersionUID = -6562668995364004814L;
	
	public SimpleTablesMenuAction() {
	}

	public SimpleTablesMenuAction(String name) {
		super(name);
		// TODO AutAbstractManagero-generated constructor stub
	}

	public SimpleTablesMenuAction(String name, Icon icon) {
		super(name, icon);
		// TODO Auto-generated constructor stub
	}
	
	public SimpleTablesMenuAction(T entity) {
		String name = new String();
		if(entity.getClass().isInstance(new ContactTypeEntity()))
			name = "Tipo Contatti";
		else if(entity.getClass().isInstance(new ActivityTypeEntity()))
			name = "Tipo Attivita";
		else if(entity.getClass().isInstance(new SectorEntity()))
			name = "Settori";
		putValue(NAME, name);
		putValue(SHORT_DESCRIPTION, "Some short description");
		this.entity = entity;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		SimpleTablesController ctrl = new SimpleTablesController(entity);
		ctrl.viewList();
		
	}

}