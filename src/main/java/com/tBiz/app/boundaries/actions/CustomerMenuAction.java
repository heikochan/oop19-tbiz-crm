package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.CustomerController;

public class CustomerMenuAction extends AbstractAction {

	private static final long serialVersionUID = -6562668995364004814L;

	public CustomerMenuAction() {
		putValue(NAME, "Clienti");
		putValue(SHORT_DESCRIPTION, "Lista di tutti i clienti");
	}

	public CustomerMenuAction(String name) {
		super(name);
	}

	public CustomerMenuAction(String name, Icon icon) {
		super(name, icon);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CustomerController cust = new CustomerController();
		cust.addFilter("acquired", true);
		cust.viewList();
	}

}