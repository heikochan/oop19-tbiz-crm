package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.CustomerController;

public class LeadMenuAction extends AbstractAction {

	private static final long serialVersionUID = -6562668995364004814L;

	public LeadMenuAction() {
		putValue(NAME, "Leads");
		putValue(SHORT_DESCRIPTION, "Some short description");
	}

	public LeadMenuAction(String name) {
		super(name);
	}

	public LeadMenuAction(String name, Icon icon) {
		super(name, icon);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		CustomerController cust = new CustomerController();
		cust.addFilter("acquired", false);
		cust.viewList();

	}

}