package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.ActivityController;

public class ActivityListMenuAction extends AbstractAction {

	private static final long serialVersionUID = 4561899148487215593L;

	public ActivityListMenuAction() {
		super();
		putValue(NAME, "Lista Attività");
		putValue(SHORT_DESCRIPTION, "Lista attività associate a tutti i clienti");
	}

	public ActivityListMenuAction(String name) {
		super(name);
	}

	public ActivityListMenuAction(String name, Icon icon) {
		super(name, icon);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ActivityController act = new ActivityController();
		act.viewList();
	}

}
