package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.ActivityController;

public class DailyActivityMenuAction extends AbstractAction {

	private static final long serialVersionUID = 4561899148487215593L;

	public DailyActivityMenuAction() {
		super();
		putValue(NAME, "Lista Attività giornaliere");
		putValue(SHORT_DESCRIPTION, "Lista attività giornaliereassociate a tutti i clienti");
	}

	public DailyActivityMenuAction(String name) {
		super(name);
	}

	public DailyActivityMenuAction(String name, Icon icon) {
		super(name, icon);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ActivityController act = new ActivityController();
		act.viewDailyList();
	}

}
