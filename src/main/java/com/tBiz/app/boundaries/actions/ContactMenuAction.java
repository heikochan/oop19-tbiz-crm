package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.ContactController;

public class ContactMenuAction extends AbstractAction {

	private static final long serialVersionUID = -6562668995364004814L;

	public ContactMenuAction() {
		putValue(NAME, "Contatti");
		putValue(SHORT_DESCRIPTION, "Some short description");
	}

	public ContactMenuAction(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public ContactMenuAction(String name, Icon icon) {
		super(name, icon);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String[] columnNames = { "First Name", "Last Name", "Sport", "# of Years", "Vegetarian" };

		Object[][] data = { { "Kathy", "Smith", "Snowboarding", new Integer(5), new Boolean(false) },
				{ "John", "Doe", "Rowing", new Integer(3), new Boolean(true) },
				{ "Sue", "Black", "Knitting", new Integer(2), new Boolean(false) },
				{ "Jane", "White", "Speed reading", new Integer(20), new Boolean(true) },
				{ "Joe", "Brown", "Pool", new Integer(10), new Boolean(false) } };
		/*
		 * String s = ("Action event detected: \n" + "    Event source: " +
		 * e.getSource() + "\n"); JScrollPane scrollPane = new JScrollPane();
		 * getContentPane().add(scrollPane); JTable table = new JTable(data,
		 * columnNames); scrollPane.add(table);
		 */
		// getContentPane().add(table);
		// textArea.append(s);

		System.out.println("Contact Click");

		ContactController contactCtrl = new ContactController();
		contactCtrl.viewList();

	}

}