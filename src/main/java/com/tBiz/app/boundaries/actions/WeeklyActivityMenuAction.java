package com.tBiz.app.boundaries.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import com.tBiz.app.controllers.ActivityController;

public class WeeklyActivityMenuAction extends AbstractAction {

	private static final long serialVersionUID = 4561899148487215593L;

	public WeeklyActivityMenuAction() {
		super();
		putValue(NAME, "Lista Attività Settimanali");
		putValue(SHORT_DESCRIPTION, "Lista attività settimanaliassociate a tutti i clienti");
	}

	public WeeklyActivityMenuAction(String name) {
		super(name);
	}

	public WeeklyActivityMenuAction(String name, Icon icon) {
		super(name, icon);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ActivityController act = new ActivityController();
		act.viewWeeklyList();
	}

}
