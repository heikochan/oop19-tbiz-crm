package com.tBiz.app.boundaries.listScreen;

interface ListViewInterface {

	public void drawList(Object[][] data, Object[] columns);

}
