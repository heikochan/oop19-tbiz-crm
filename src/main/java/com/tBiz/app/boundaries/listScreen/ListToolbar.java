package com.tBiz.app.boundaries.listScreen;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import com.tBiz.app.boundaries.DateFilter;
import com.tBiz.app.boundaries.TextFieldWithPlaceholder;
import com.tBiz.app.boundaries.defaultConfigs.DefaultObjectValues;
import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.entities.AbstractEntity;

public class ListToolbar extends JToolBar {
	JButton btnAddEntity = new JButton("Aggiungi");
	JButton btnUpdateEntity = new JButton("Modifica");
	JButton btnDeleteEntity = new JButton("Elimina");
	TextFieldWithPlaceholder txtSearch = new TextFieldWithPlaceholder();
	JButton btnSearch = new JButton("Cerca");
	JLabel lblSearched = new JLabel("");
	DateFilter dateFilter;
	protected ListViewPanel parent;

	private Font buttonFont = (Font) DefaultObjectValues.BUTTONS_FONT.getValue();
	private Insets buttonMargin = (Insets) DefaultObjectValues.BUTTONS_MARGIN.getValue();

	public <T extends AbstractController, E extends AbstractEntity> ListToolbar(ListViewPanel<T, E> parent) {
		this.parent = parent;

		btnAddEntity.setFont(buttonFont);
		btnAddEntity.setMargin(buttonMargin);

		btnUpdateEntity.setFont(buttonFont);
		btnUpdateEntity.setMargin(buttonMargin);

		btnDeleteEntity.setFont(buttonFont);
		btnDeleteEntity.setMargin(buttonMargin);

		txtSearch.setPlaceholder("Search...");
		txtSearch.setColumns(10);
		lblSearched.setVisible(false);
		lblSearched.setToolTipText("Clicca per eliminare il criterio");

		add(btnAddEntity);
		add(btnUpdateEntity);
		add(btnDeleteEntity);
		addSeparator();
		add(txtSearch);
		add(btnSearch);
		add(lblSearched);

		btnAddEntity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.controller.addNew();
			}
		});

		btnUpdateEntity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!(parent.table.getSelectionModel().isSelectionEmpty())) {
					parent.controller.viewDetails(getSelectedRowIdEntity());
				} else {
					JOptionPane.showMessageDialog(null, "Non è stato selezionato nessun record");
				}
			}
		});

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!txtSearch.getText().isEmpty()
						&& !txtSearch.getText().equalsIgnoreCase(txtSearch.getPlaceholder())) {
					parent.controller.setSearch(txtSearch.getText());
					parent.controller.viewList();
					lblSearched.setText("Ricerca: " + "contiene " + "%" + txtSearch.getText() + "%");
					lblSearched.setVisible(true);
					repaint();
				} else {
					JOptionPane.showMessageDialog(null, "Indicare il testo da ricercare");
				}
			}
		});

		lblSearched.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				lblSearched.setText("");
				lblSearched.setVisible(false);
				txtSearch.setText("");
				parent.repaint();
				parent.controller.clearSearch();
				parent.controller.viewList();
			}
		});

		btnDeleteEntity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!(parent.table.getSelectionModel().isSelectionEmpty())) {
					parent.controller.delete(parent.controller.getModel().find(getSelectedRowIdEntity()));
					parent.controller.viewList();
				} else {
					JOptionPane.showMessageDialog(null, "Non è stato selezionato nessun record");
				}
			}
		});
	}

	public void setDateFilter(DateFilter dateFilter) {
		this.dateFilter = dateFilter;
		add(dateFilter);
		this.dateFilter.setVisible(true);
		this.repaint();
	}

	public void removeDateFilter() {
		this.dateFilter = null;
		this.repaint();
	}

	public DateFilter getDateFilter() {
		return this.dateFilter;
	}

	private int getSelectedRowIdEntity() {
		int id = 0;
		int i = 0;
		String colName;
		for (i = 0; i < parent.table.getColumnCount(); i++) {
			colName = parent.table.getColumnName(i);
			System.out.println(colName);
			if (colName.toLowerCase().trim().equalsIgnoreCase("codice"))
				break;
		}
		id = Integer.parseInt(parent.table.getValueAt(parent.table.getSelectedRow(), i).toString());
		return id;
	}
}
