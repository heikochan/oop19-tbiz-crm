package com.tBiz.app.boundaries.listScreen;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;

import com.tBiz.app.boundaries.DateFilter;
import com.tBiz.app.boundaries.ViewOnlyTable;
import com.tBiz.app.boundaries.defaultConfigs.DefaultObjectValues;
import com.tBiz.app.controllers.AbstractController;
import com.tBiz.app.entities.AbstractEntity;

public class ListViewPanel<T extends AbstractController, E extends AbstractEntity> extends JInternalFrame
		implements ListViewInterface {

	private JScrollPane scrollPane;
	protected ViewOnlyTable table;
	protected ListToolbar toolbar;
	public T controller;
	protected E entity;

	private Font tableFont = (Font) DefaultObjectValues.DEFAULT_FONT.getValue();

	public ListViewPanel(T controller, String name, boolean resize, boolean close, boolean maximize,
			boolean reducetoicon) {
		super(name, resize, close, maximize, reducetoicon);
		this.controller = controller;
		setSize(200, 200);
		setLocation(1, 1);
		setBounds(1, 1, 200, 200);
		getContentPane().setLayout(new BorderLayout(0, 0));

		toolbar = new ListToolbar(this);
		toolbar.setVisible(true);
		toolbar.setRollover(true);
		getContentPane().add(toolbar, BorderLayout.NORTH);

		scrollPane = new JScrollPane();
		// scrollPane.setLayout(new ScrollPaneLayout());
		getContentPane().add(scrollPane);

	}

	public void drawList(Object[][] data, Object[] columns) {
		this.table = new ViewOnlyTable(data, columns);
		this.table.setFont(tableFont);
		// this.table.setDefaultRenderer(Object.class, new TableRenderer());

		scrollPane.setViewportView(this.table);
		scrollPane.setVisible(true);

		scrollPane.updateUI();
		table.setFillsViewportHeight(true);
		table.setVisible(true);
		// this.getContentPane().setLayout(new BorderLayout());
		getContentPane().add(scrollPane);
		updateUI();
	}

	public void activateDateFilter() {
		if (toolbar.getDateFilter() == null) {
			DateFilter dtFilter = new DateFilter(this);
			toolbar.setDateFilter(dtFilter);
		}
	}

}
