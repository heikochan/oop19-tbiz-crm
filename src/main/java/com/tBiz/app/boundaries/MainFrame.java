package com.tBiz.app.boundaries;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.Action;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import com.tBiz.app.boundaries.actions.ActivityListMenuAction;
import com.tBiz.app.boundaries.actions.ContactMenuAction;
import com.tBiz.app.boundaries.actions.CustomerMenuAction;
import com.tBiz.app.boundaries.actions.DailyActivityMenuAction;
import com.tBiz.app.boundaries.actions.LeadMenuAction;
import com.tBiz.app.boundaries.actions.SimpleTablesMenuAction;
import com.tBiz.app.boundaries.defaultConfigs.DefaultObjectValues;
import com.tBiz.app.entities.ActivityTypeEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.SectorEntity;

/**
 * Application MainFrame contains DesktopPane and MenuBar
 *
 */
public class MainFrame extends JFrame {
	private final Action customerAction = new CustomerMenuAction();
	private final Action leadAction = new LeadMenuAction();
	private final Action contactAction = new ContactMenuAction();
	private final Action activityListAction = new ActivityListMenuAction();
	private final Action dailyActivityListAction = new DailyActivityMenuAction();

	private JDesktopPane desktopPane;
	public JMenuBar menuBar;

	// class constructor
	public MainFrame() throws HeadlessException {
		// set the LookAndFeel
		try {
			String lfClass = UIManager.getCrossPlatformLookAndFeelClassName();
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					lfClass = info.getClassName();
					break;
				}
			}
			UIManager.setLookAndFeel(lfClass);
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		// other settings
		setFont((Font) DefaultObjectValues.DEFAULT_FONT.getValue());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("tBiz CRM - Software gestionale per clienti e leads");
		// maximize the frame
		this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);

		// instantiate the DesktopPane and add to the MainFrame
		desktopPane = new JDesktopPane();
		desktopPane.setSize(500, 500);
		desktopPane.setLayout(new BorderLayout());
		getContentPane().add(desktopPane, BorderLayout.CENTER);

		setVisible(true);
	}

	public MainFrame(GraphicsConfiguration gc) {
		super(gc);
	}

	public MainFrame(String title) throws HeadlessException {
		super(title);
	}

	public MainFrame(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

	/**
	 * setup the main Menu
	 */
	public void setupMenu() {

		// instantiate the menubar
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		// instantiate the menu voice and add to the menuBar
		JMenu companyDataMenu = new JMenu("Gestione Anagrafiche");
		menuBar.add(companyDataMenu);
		menuBar.setFont((Font) DefaultObjectValues.BUTTONS_FONT.getValue());

		// instantiate the contained items
		JMenuItem customersSubmenu = new JMenuItem("Clienti");
		customersSubmenu.setAction(customerAction);
		companyDataMenu.add(customersSubmenu);

		JMenuItem leadsSubmenu = new JMenuItem("Lead");
		leadsSubmenu.setAction(leadAction);
		companyDataMenu.add(leadsSubmenu);

		JMenuItem contactsSubmenu = new JMenuItem("Contatti");
		contactsSubmenu.setAction(contactAction);
		companyDataMenu.add(contactsSubmenu);

		JMenu activityMenu = new JMenu("Gestione Attività");
		menuBar.add(activityMenu);

		JMenuItem activityListSubmenu = new JMenuItem("Lista Attività");
		activityListSubmenu.setAction(activityListAction);
		activityMenu.add(activityListSubmenu);

		// JMenu calendarSubmenu = new JMenu("Calendario");
		// activityMenu.add(calendarSubmenu);

		JMenuItem dailyCalendarSubmenu = new JMenuItem("Giornaliero");
		dailyCalendarSubmenu.setAction(dailyActivityListAction);
		activityMenu.add(dailyCalendarSubmenu);

		// JMenuItem weeklyCalendarSubmenu = new JMenuItem("Settimanale");
		// weeklyCalendarSubmenu.setAction(weeklyActivityListAction);
		// calendarSubmenu.add(weeklyCalendarSubmenu);

		JMenu settingsMenu = new JMenu("Configurazioni");
		menuBar.add(settingsMenu);

		JMenuItem contactTypesSubmenu = new JMenuItem("Tipi contatti");
		Action cta = new SimpleTablesMenuAction(new ContactTypeEntity());
		contactTypesSubmenu.setAction(cta);
		settingsMenu.add(contactTypesSubmenu);

		JMenuItem activityTypesSubmenu = new JMenuItem("Tipi attività");
		Action ata = new SimpleTablesMenuAction(new ActivityTypeEntity());
		activityTypesSubmenu.setAction(ata);
		settingsMenu.add(activityTypesSubmenu);

		JMenuItem sectorsSubmenu = new JMenuItem("Settori lavorativi");
		Action sta = new SimpleTablesMenuAction(new SectorEntity());
		sectorsSubmenu.setAction(sta);
		settingsMenu.add(sectorsSubmenu);

	}

	/**
	 * return the Desktop object
	 * 
	 * @return MenuFrame's desktopPane
	 */
	public JDesktopPane getDesktop() {
		return desktopPane;
	}

	/**
	 * Check and return the frame if already open
	 * 
	 * @param title use the title to recognize the frame
	 * @return JInternalFrame if found
	 */
	public <T extends JInternalFrame> T getAlreadyOpen(String title) {
		// instantiate to null
		T found = null;
		// for earch opened frame in the desktopPane
		for (JInternalFrame form : this.getDesktop().getAllFrames()) {
			// check if has the same title
			if (form.getTitle().equals(title)) {
				// if found, retrive
				found = (T) form;
				// and break
				break;
			}
		}

		return found;
	}
}
