package com.tBiz.app.boundaries;

import java.awt.Color;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.tBiz.app.boundaries.detailsScreen.DetailViewPanel;
import com.tBiz.app.controllers.SimpleTablesController;
import com.tBiz.app.entities.AbstractEntity;
import com.tBiz.app.entities.ContactTypeEntity;
import com.tBiz.app.entities.repositories.ContactTypeRepository;

/**
 * Base panel to manage simple tables with only code (not editable) and  
 * description
 */
public class SimpleTableDetailsPanel extends DetailViewPanel {
	private JPanel panel = new JPanel();
	private JTextField txtCode = new JTextField();
	private JTextField txtDescription = new JTextField();

	private static final long serialVersionUID = 5780502524459484585L;

	/**
	 * Class constructor
	 * works on SimpleTableController and AbstractEntity
	 * @param ctrl		SimpleTableController to manage database communication
	 * @param entity	AbstractEntity to manage entitys' informations
	 */
	public SimpleTableDetailsPanel(SimpleTablesController ctrl, AbstractEntity entity) {
		super(entity.getClass().getName(), true, true, true, true);
		
		// retrive entity and controller
		this.entity = entity;
		this.controller = ctrl;
		
		// draw the panel
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
						FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
						FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
				new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
						FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
						FormSpecs.DEFAULT_ROWSPEC, }));
	}

	@Override
	/**
	 * drawDetails
	 * insert graphic objects in panel
	 */
	public void drawDetails() {
		// Entity Code
		JLabel lblCode = new JLabel("Codice");
		this.txtCode = new JTextField();
		contentPanel.add(lblCode, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(txtCode, this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtCode.setColumns(this.TEXTFIELD_COLUMNS);
		this.txtCode.setText(String.valueOf(this.entity.getId()));
		// not enabled
		this.txtCode.setEnabled(false);

		this.rowStart += 2;
		
		// Estringntity Description
		JLabel lblDesc = new JLabel("Descrizione");
		this.txtDescription = new JTextField();
		contentPanel.add(lblDesc, this.label_col1_start + ", " + this.rowStart + ", " + this.defaultLabelPosition);
		contentPanel.add(this.txtDescription , this.field_col1_start + ", " + this.rowStart + ", " + this.defaultFieldPosition);
		this.txtDescription.setColumns(this.TEXTFIELD_COLUMNS);
		// use the generic getDescription, each Entity class implement this mehod
		this.txtDescription.setText(this.entity.getDescription());
		
		// update the panel
		this.repaint();
	}

	@Override
	/**
	 * save the informations
	 */
	public void saveEntity() {
		this.entity.setDescription(this.txtDescription.getText());
		this.controller.saveDetails(this.entity);
	}
}
