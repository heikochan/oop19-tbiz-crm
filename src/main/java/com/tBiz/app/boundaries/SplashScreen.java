package com.tBiz.app.boundaries;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

public class SplashScreen extends JWindow {
	private JLabel lblTehnologicalBusinessCrm = new JLabel("<html><p>Technological</p><p>Business</p><p>CRM</p></html>");

	public SplashScreen(JFrame frame) {
		super(frame);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 450, 0 };
		gridBagLayout.rowHeights = new int[] { 300, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		getContentPane().add(panel, gbc_panel);
		panel.setLayout(null);
		lblTehnologicalBusinessCrm.setBounds(66, 136, 317, 100);

		lblTehnologicalBusinessCrm.setForeground(Color.BLACK);
		lblTehnologicalBusinessCrm.setFont(new Font("EB Garamond 12", Font.BOLD, 26));
		panel.add(lblTehnologicalBusinessCrm);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 450, 300);
		lblNewLabel.setIcon(
				new ImageIcon("./src/main/resources/splash-screen.jpg"));
		panel.add(lblNewLabel);

	}

}
