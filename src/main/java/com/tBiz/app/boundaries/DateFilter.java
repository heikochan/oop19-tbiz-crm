package com.tBiz.app.boundaries;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.tBiz.app.boundaries.listScreen.ListViewPanel;

/**
 * Graphic object for filter pourpose added to the panel toolbar, show a
 * datepicker field and two directional buttons to move up forward or down
 * backward on dates *
 */
public class DateFilter extends JPanel {
	private JDatePickerImpl dtFilter = null;
	private JButton moveNext = new JButton(">>");
	private JButton movePrev = new JButton("<<");
	protected ListViewPanel parent;

	public DateFilter(ListViewPanel parent) {
		// properties for calendar object
		Properties p = new Properties();
		p.put("text.today", "Oggi");
		p.put("text.month", "Mese");
		p.put("text.year", "Anno");
		this.parent = parent;

		// declare the model used in calendard
		UtilDateModel firstDtModel = new UtilDateModel();
		JDatePanelImpl firstDtPanel = new JDatePanelImpl(firstDtModel, p);
		// set the datepicker object
		dtFilter = new JDatePickerImpl(firstDtPanel, new DateLabelFormatter());

		// add objects to panel
		this.add(movePrev);
		this.add(dtFilter);
		this.add(moveNext);

		JButton activateFilter = new JButton("Visualizza attività");
		add(activateFilter);

		// sets the action listeners
		// onclick down backward
		movePrev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dtFilter.getModel().setDay(dtFilter.getModel().getDay() - 1);
			}
		});

		// onclick up forward
		moveNext.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dtFilter.getModel().setDay(dtFilter.getModel().getDay() + 1);
			}
		});

		// activate filter by inserted date
		activateFilter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.controller.removeFilter("startDate");
				// get LocalDate from object
				String dateVal = dtFilter.getJFormattedTextField().getText();
				LocalDate localDate = DateLabelFormatter.formatToLocalDate(dateVal, "dd-MM-yyyy");
				LocalDateTime ldtIni = LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
				// add the new date to the filter
				parent.controller.addFilter("startDate", ldtIni);
				// update the list
				parent.controller.viewList();
			}

		});
	}
}
